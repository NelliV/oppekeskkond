<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Varasalv extends Model
{
    protected $fillable = ['kategooria', 'pealkiri', 'lyhikirjeldus', 'tekst'];
}
