<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Lastekasvatus extends Controller
{
    function index()
    {
        $data= DB::table('varasalvs')
            ->where('kategooria','=','Lastekasvatus')
            ->paginate(6);
        return view('varasalv/lastekasvatus', ['data'=>$data]);
    }

    public function show($id)
    {
        $data = Varasalv::findOrFail($id);
    
        return view('varasalv/show', compact('data'));
    }
}