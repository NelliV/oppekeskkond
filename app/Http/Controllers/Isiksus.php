<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Isiksus extends Controller
{
    function index()
    {
        $data= DB::table('varasalvs')
            ->where('kategooria','=','Isiksus')
            ->paginate(6);
        return view('varasalv/isiksus', ['data'=>$data]);
    }

    public function show($id)
    {
        $data = Varasalv::findOrFail($id);
    
        return view('varasalv/show', compact('data'));
    }
}