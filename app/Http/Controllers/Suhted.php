<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Suhted extends Controller
{
    function index()
    {
        $data= DB::table('varasalvs')
            ->where('kategooria','=','Suhted')
            ->paginate(6);
        return view('varasalv/suhted', ['data'=>$data]);
    }

    public function show($id)
    {
        $data = Varasalv::findOrFail($id);
    
        return view('varasalv/show', compact('data'));
    }
}