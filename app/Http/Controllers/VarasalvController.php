<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Varasalv;

class VarasalvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $varasalvs = Varasalv::all();
    
         return view('varasalv/index', compact('varasalvs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('varasalv/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
             'kategooria' => 'required|max:255',
             'pealkiri' => 'required|max:255',
             'lyhikirjeldus' => 'required|max:255',
             'tekst' => 'required|max:30000',
         ]);
         $varasalv = Varasalv::create($validatedData);

         
    
         return redirect('/varasalvs')->with('success', 'Õpetus on edukalt salvestatud');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $varasalv = Varasalv::findOrFail($id);
        return view('varasalv/show', compact('varasalv'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $varasalv = Varasalv::findOrFail($id);
    
        return view('varasalv/edit', compact('varasalv'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'kategooria' => 'required|max:255',
            'pealkiri' => 'required|max:255',
            'lyhikirjeldus' => 'required|max:255',
            'tekst' => 'required|max:30000',
        ]);
        Varasalv::whereId($id)->update($validatedData);

        return redirect('/varasalvs')->with('success', 'Õpetus on edukalt uuendatud');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $varasalv = Varasalv::findOrFail($id);
        $varasalv->delete();

        return redirect('/varasalvs')->with('success', 'Õpetus on edukalt kustutatud');
    }
}
