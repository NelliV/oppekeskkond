<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Kirik extends Controller
{
    function index()
    {
        $data= DB::table('varasalvs')
            ->where('kategooria','=','Kirik')
            ->paginate(6);
        return view('varasalv/kirik', ['data'=>$data]);
    }

    public function show($id)
    {
        $data = Varasalv::findOrFail($id);
    
        return view('varasalv/show', compact('data'));
    }
}