<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AlfabeetController extends Controller
{
    public function function(){
        $q = Input::get ( 'q' );
        if($q != ""){
            $varasalv = Varasalv::where('pealkiri','LIKE','%'.$q.'%')
                                    ->orWhere('lyhikirjeldus','LIKE','%'.$q.'%')
                                    ->get();
            if(count($varasalv) > 0)
                return view('varasalv/alfabeet')->withDetails($varasalv)->withQuery($q);
        }
        return view('varasalv/alfabeet')->withMessage("Ei leidnud tulemusi...");
    }
}




