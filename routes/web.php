<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Input;
use App\Varasalv;

Auth::routes();

Route::group(['middleware' => ['web','auth']], function(){
    Route::get('/', function () {
        return view('sissejuhatus/ajalugu');
    });

    Route::get('/sissejuhatus/ajalugu', function() {
        if (Auth::user()->admin == 0) {
            return view('sissejuhatus/ajalugu');
        } else {
            $users['users'] = \App\User::all();
            return view('adminsissejuhatus/ajalugu', $users);
        }
    });

    Route::get('/sissejuhatus/ristimine', function() {
        if (Auth::user()->admin == 0) {
            return view('sissejuhatus/ristimine');
        } else {
            $users['users'] = \App\User::all();
            return view('adminsissejuhatus/ristimine', $users);
        }
    });

    Route::get('/sissejuhatus/armulaud', function() {
        if (Auth::user()->admin == 0) {
            return view('sissejuhatus/armulaud');
        } else {
            $users['users'] = \App\User::all();
            return view('adminsissejuhatus/armulaud', $users);
        }
    });

    Route::get('/sissejuhatus/palve', function() {
        if (Auth::user()->admin == 0) {
            return view('sissejuhatus/palve');
        } else {
            $users['users'] = \App\User::all();
            return view('adminsissejuhatus/palve', $users);
        }
    });

    Route::get('/sissejuhatus/piibel', function() {
        if (Auth::user()->admin == 0) {
            return view('sissejuhatus/piibel');
        } else {
            $users['users'] = \App\User::all();
            return view('adminsissejuhatus/piibel', $users);
        }
    });

    Route::get('/sissejuhatus/10kasku', function() {
        if (Auth::user()->admin == 0) {
            return view('sissejuhatus/10kasku');
        } else {
            $users['users'] = \App\User::all();
            return view('adminsissejuhatus/10kasku', $users);
        }
    });

    Route::get('/sissejuhatus/usutunnistus', function() {
        if (Auth::user()->admin == 0) {
            return view('sissejuhatus/usutunnistus');
        } else {
            $users['users'] = \App\User::all();
            return view('adminsissejuhatus/usutunnistus', $users);
        }
    });

    Route::get('/kkk', function () {
        return view('kkk');
    });

    Route::get('/admin', function() {
        if (Auth::user()->admin == 0) {
            return view('sissejuhatus/ajalugu');
        } else {
            $users['users'] = \App\User::all();
            return view('admin', $users);
        }
    });


    Route::get('/varasalv', function () {
        return view('varasalv/varasalv');
    });

    Route::get('/varasalv/create', function () {
        return view('varasalv/create');
    });

    /* Varasalv middle pages */
    Route::get('varasalv/abielu', "Abielu@index");
    Route::get('varasalv/elujasurm', "Elujasurm@index");
    Route::get('varasalv/isiksus', "Isiksus@index");
    Route::get('varasalv/kirik', "Kirik@index");
    Route::get('varasalv/lastekasvatus', "Lastekasvatus@index");
    Route::get('varasalv/suhted', "Suhted@index");

    /* Varasalv individual pages */
    Route::resource('varasalvs', 'VarasalvController');

    /* Search function in Varasalv */
    Route::any('/search',function(){
        $q = Input::get ( 'q' );
        if($q != ""){
            $varasalv = Varasalv::where('pealkiri','LIKE','%'.$q.'%')
                                    ->orWhere('lyhikirjeldus','LIKE','%'.$q.'%')
                                    ->get();
            if(count($varasalv) > 0)
                return view('varasalv/search')->withDetails($varasalv)->withQuery($q);
        }
        return view('varasalv/search')->withMessage("Ei leidnud tulemusi...");
    });

    Route::get('/varasalv/search', function () {
        return view('varasalv/search');
    });

    //Route::get('/home', 'HomeController@home')->name('home');
    //Route::any('/varasalv/alfabeet', AlfabeetController@alfabeet)->name('alfabeet');
});