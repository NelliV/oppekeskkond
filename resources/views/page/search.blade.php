@if (count($items) === 0)
... html showing no articles found
@elseif (count($items) >= 1)
... print out results
    @foreach($items as $item)
    print item
    @endforeach
@endif