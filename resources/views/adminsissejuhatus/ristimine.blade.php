@extends('layouts.app')

@section('content')
<!-- RISTIMINE --> 
    @if(Auth::check())
    <div class="card-body shadow">
        <div class="tab-pane show fade">
            <div class="row" style="padding: 1.5em;">
                <div class="col-md-6 embed-responsive embed-responsive-16by9" >
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VuvQYZSeza0"  allowfullscreen></iframe>
                </div>
                <div class="col-md-6 text-center" >
                    <a style="margin: 2em; background-color:rgba(0,0,0,.09);"  data-toggle="modal" data-target="#test" class="btn btn-outline-success" href="test">Testi ennast!</a>
                    <!-- Siin on Ristimise test -->
                    <div class = "modal fade" id = "test" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
                        <div class = "modal-dialog">
                            <div class = "modal-content">
                                <div class = "modal-header">
                                    <h4 class = "modal-title" id = "myModalLabel">
                                        Ristimise test
                                    </h4>
                                    <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                        &times;
                                    </button>
                                </div>
                                <div class = "modal-body">            
                                    <label>1. Lorem ipsum õige/vale väide küsimus</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right1" role="button">
                                            Õige
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong1" role="button">
                                            Vale
                                        </a>
                                    </p>
                                    <div id="accordion1">
                                        <div class="collapse" id="right1" data-parent="#accordion1">
                                            <div class="card card-body">
                                                See on tõesti nii ja siia ma saan ka seletada, miks see niimoodi on.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong1" data-parent="#accordion1">
                                            <div class="card card-body">
                                                See on väär vastus ja siin ma seletan, mis see siis väär on.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>2. Lorem ipsum õige/vale väide küsimus</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right2" role="button">
                                            Õige
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong2" role="button">
                                            Vale
                                        </a>
                                    </p>
                                    <div id="accordion2">
                                        <div class="collapse" id="right2" data-parent="#accordion2">
                                            <div class="card card-body">
                                                See on tõesti nii ja siia ma saan ka seletada, miks see niimoodi on.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong2" data-parent="#accordion2">
                                            <div class="card card-body">
                                                See on väär vastus ja siin ma seletan, mis see siis väär on.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>3. Lorem ipsum õige/vale väide küsimus</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right3" role="button">
                                            Õige
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong3" role="button">
                                            Vale
                                        </a>
                                    </p>
                                    <div id="accordion3">
                                        <div class="collapse" id="right3" data-parent="#accordion3">
                                            <div class="card card-body">
                                                See on tõesti nii ja siia ma saan ka seletada, miks see niimoodi on.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong3" data-parent="#accordion3">
                                            <div class="card card-body">
                                                See on väär vastus ja siin ma seletan, mis see siis väär on.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>4. Lorem ipsum õige/vale väide küsimus</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right4" role="button">
                                            Õige
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong4" role="button">
                                            Vale
                                        </a>
                                    </p>
                                    <div id="accordion4">
                                        <div class="collapse" id="right4" data-parent="#accordion4">
                                            <div class="card card-body">
                                                See on tõesti nii ja siia ma saan ka seletada, miks see niimoodi on.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong4" data-parent="#accordion4">
                                            <div class="card card-body">
                                                See on väär vastus ja siin ma seletan, mis see siis väär on.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>5. Lorem ipsum õige/vale väide küsimus</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right5" role="button">
                                            Õige
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong5" role="button">
                                            Vale
                                        </a>
                                    </p>
                                    <div id="accordion5">
                                        <div class="collapse" id="right5" data-parent="#accordion5">
                                            <div class="card card-body">
                                                See on tõesti nii ja siia ma saan ka seletada, miks see niimoodi on.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong5" data-parent="#accordion5">
                                            <div class="card card-body">
                                                See on väär vastus ja siin ma seletan, mis see siis väär on.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-footer">
                                    <button type = "button" class = "btn btn-default" data-dismiss = "modal">
                                        Tagasi
                                    </button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </div>
            </div>
        </div>
        <div class="row" style="padding: 1.5em;">
            <p>Ristimisel saab inimene kiriku liikmeks. Kuuludes jumalarahva perekonda, 
            on ristitud inimene kutsutud kasvama usus, Jumala tundmises ning järgima Jeesuse 
            õpetust. Lapse ristimisel võtavad vanemad ja ristivanemad endale kohustuse kasvatada 
            last kristlikus usus.</p>
            <p>Ristimine toimetatakse veega Isa ja Poja ja Püha Vaimu nimesse. Ristimiseks 
            kasutatakse puhast vett, mida pannakse ristitava pähe. Last võib ristida juba 
            päris esimestel elukuudel. Täiskasvanu ristimiseks ei ole ealisi piiranguid. 
            Kui inimene on jõudnud leeriikka ja ta on lapsena jäänud ristimata, toimub tema 
            ristimine koos konfirmatsiooniga.</p>
            <h4>Ristivanem</h4>
            <p>Lapsel peab ristimiseks olema vähemalt üks ristivanem, kes on ise ristitud ja 
            konfirmeeritud ning vähemalt üks tema vanematest peab samuti olema ristitud ja 
            konfirmeeritud. Erandkorras võib ristivanem kuuluda ka teise konfessiooni. 
            Ka täiskasvanul võib olla ristivanem, kuid see ei ole kohustuslik.</p>
            <p>Ristivanema ülesanne on aidata perekonda (vajadusel vanemate puudumisel 
            nende asemel) lapse kristlikul kasvatamisel, et ristitud laps võiks saada 
            eakohast ristimisõpetust ning kasvada jumalakartuses ja -armastuses. Ristivanem 
            on ristimistalituse tunnistajaks ja saab vajadusel tõestada, et tema ristilaps 
            on ristitud. Ristivanem annab talitusel lubaduse palvetada ristilapse eest ning 
            aidata teda oma sõna ja eeskujuga, et ta võiks püsida kristliku Kiriku elava 
            liikmena, armastades Jumalat ja ligimest nii, nagu Kristus meid on õpetanud. 
            Kogudus toetab omalt poolt laste- ja noorsootöö ning leerikooli kaudu lapse 
            usulist kasvamist.</p>
            <h4>Ristimise koht ja aeg</h4>
            <p>Ristimine peetakse üldjuhul kirikus või kabelis seal asuva ristimiskivi 
            või altari juures. Võimalik on ristida ka kodus või muus sobivas kohas. 
            Ristimise aeg ja koht lepitakse kokku ristimist teostava vaimulikuga. 
            Enne ristimist on soovitatav vanematel ja ristivanematel koos ristitava 
            lapsega kohtuda vaimulikuga. Ristimistalituse eest on sobilik tasuda 
            kogudusele ristimisannetus.</p>
            <h4>Kes ristib?</h4>
            <p>Ristimise õigus on igal luterliku kiriku vaimulikul. Ristimist on soovitatav 
            taotleda oma elukohajärgse kogudusest. Juhul kui lapse või täiskasvanu elu on 
            ohus ja soovitakse tema ristimist, võib seda teostada ka teine ristitud inimene. 
            Sellist ristimist nimetatakse hädaristimisteks ja see kinnitatakse hiljem 
            koguduse õpetaja poolt.</p>
            <h4>Ristimise rõivastus ja kaelarist</h4>
            <p>Ristimisriietus on valge. Laste puhul on võimalik kasutada ristimiskleiti, 
            täiskasvanute jaoks on mitmes koguduses kasutusel valged pikad ristimisrüüd.</p>
            <p>Traditsiooniliselt on ristimisel saanud ristitav endale kaelaristi. Soovitatavalt 
            on see väärismetallist (hõbedast või kullast) ketiga rist, mis talitusel 
            õnnistatakse ja kaela asetatakse. Laste puhul on kaelarist sageli ka kingitus 
            ristivanemate poolt.</p>
            <h4>Mis saab siis, kui ma ei mäleta oma ristimist?</h4>
            <p>Ristimine kehtib ka siis, kui inimene ise oma ristimist ei mäleta. Kui inimene 
            ei tea kindlalt, kas ta on ristitud või mitte ja seda ei ole võimalik ka kusagilt 
            järele kontrollida, võidakse läbi viia niinimetatud tingristimine.</p>
        </div>
    </div>
    @endif
@endsection


