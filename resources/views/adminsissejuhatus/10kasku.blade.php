@extends('layouts.app')

@section('content')

<!-- 10 KÄSKU --> 
@if(Auth::check())  
<div class="card-body shadow">    
    <div class="tab-pane show fade">
        <div class="row" style="padding: 1.5em;">
            <div class="col-md-6 embed-responsive embed-responsive-16by9" >
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/TCkm242rxfc"  allowfullscreen></iframe>
            </div>
            <div class="col-md-6 text-center" >
                <a style="margin: 2em; background-color:rgba(0,0,0,.09);"  data-toggle="modal" data-target="#test" class="btn btn-outline-success" href="test">Testi ennast!</a>
                <!-- Siin on test -->
                <div class = "modal fade" id = "test" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
                    <div class = "modal-dialog">
                        <div class = "modal-content">
                            <div class = "modal-header">
                                <h4 class = "modal-title" id = "myModalLabel">
                                    10 Käsku enesekontrolli test
                                </h4>
                                <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                    &times;
                                </button>
                            </div>
                            <div class = "modal-body">            
                                <label>1. Kas järgnev väide on õige?: "Sina ei pea mitte Issanda, oma Jumala nime ilmaasjata suhu võtma, sest Issand ei jäta seda nuhtlemata, kes Tema nime asjata suhu võtab."</label>
                                <p>
                                    <a class="btn btn-outline-success" data-toggle="collapse" href="#right1" role="button">
                                        Õige
                                    </a>
                                    <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong1" role="button">
                                        Vale
                                    </a>
                                </p>
                                <div id="accordion1">
                                    <div class="collapse" id="right1" data-parent="#accordion1">
                                        <div class="card card-body">
                                        Õige! Meie peame Jumalat kartma ja armastama nii, et me Tema nimel ei nea, 
                                        ei vannu, ei nõiu, ei valeta ega peta, vaid et me Teda kõiges hädas appi hüüame, 
                                        palume, kiidame ja täname.
                                        </div>
                                    </div>
                                    <div class="collapse" id="wrong1" data-parent="#accordion1">
                                        <div class="card card-body">
                                            Arvasid valesti. Proovi palun uuesti.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class = "modal-body">            
                                <label>2. Kas järgev väide on õige?: "Sina ei pea üldse pühapäeva pühitsema."</label>
                                <p>
                                    <a class="btn btn-outline-success" data-toggle="collapse" href="#right2" role="button">
                                        Õige
                                    </a>
                                    <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong2" role="button">
                                        Vale
                                    </a>
                                </p>
                                <div id="accordion2">
                                    <div class="collapse" id="right2" data-parent="#accordion2">
                                        <div class="card card-body">
                                            Ei see ei ole nii. Vastasid valesti. Kolmas käsk ütleb: "Sina pead pühapäeva pühitsema."
                                        </div>
                                    </div>
                                    <div class="collapse" id="wrong2" data-parent="#accordion2">
                                        <div class="card card-body">
                                            Vastasid õigesti. Meie peame Jumalat kartma ja armastama nii, et me jutlust ja Jumala sõna ei põlga, vaid et me seda  pühaks peame ning heal meelel kuulame ja õpime.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class = "modal-body">            
                                <label>3. Kas järgnev väide on õige?: "Sina võid vahel tappa."</label>
                                <p>
                                    <a class="btn btn-outline-success" data-toggle="collapse" href="#right3" role="button">
                                        Õige
                                    </a>
                                    <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong3" role="button">
                                        Vale
                                    </a>
                                </p>
                                <div id="accordion3">
                                    <div class="collapse" id="right3" data-parent="#accordion3">
                                        <div class="card card-body">
                                            Kahjuks ei vastanud sa õigesti. Viies käsk ütleb: "Sina ei tohi tappa."
                                        </div>
                                    </div>
                                    <div class="collapse" id="wrong3" data-parent="#accordion3">
                                        <div class="card card-body">
                                            Tubli! Vastasid õigesti! Me peame Jumalat kartma ja armastama nii, et me oma ligimese ihule ega hingele ei 
                                            tee ühtki kahju ega kurja, vaid et me teda aitame ja temale head teeme kõigis ihuhädades.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class = "modal-body">            
                                <label>4. Kuues käsk ütleb: "Sina ei tohi varastada"</label>
                                <p>
                                    <a class="btn btn-outline-success" data-toggle="collapse" href="#right4" role="button">
                                        Õige
                                    </a>
                                    <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong4" role="button">
                                        Vale
                                    </a>
                                </p>
                                <div id="accordion4">
                                    <div class="collapse" id="right4" data-parent="#accordion4">
                                        <div class="card card-body">
                                            Vale vastus. Kuues käsk ütleb hoopis: "Sina ei tohi rikkuda abielu."
                                        </div>
                                    </div>
                                    <div class="collapse" id="wrong4" data-parent="#accordion4">
                                        <div class="card card-body">
                                            Arvasid õigesti. See väide oli tõesti väär. "Sina ei tohi varastada" on hoopis  seitsmes käsk.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class = "modal-body">            
                                <label>5. Sina ei pea mitte Issanda, oma Jumala nime ilmaasjata suhu võtma, sest Issand ei jäta seda nuhtlemata, kes Tema nime asjata suhu võtab.</label>
                                <p>
                                    <a class="btn btn-outline-success" data-toggle="collapse" href="#right5" role="button">
                                        Õige
                                    </a>
                                    <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong5" role="button">
                                        Vale
                                    </a>
                                </p>
                                <div id="accordion5">
                                    <div class="collapse" id="right5" data-parent="#accordion5">
                                        <div class="card card-body">
                                            Vastasid õigesti. Meie peame Jumalat kartma ja armastama nii, et me Tema nimel ei nea, ei vannu, ei nõiu, ei valeta ega peta, vaid et me Teda kõiges hädas appi hüüame, palume, kiidame ja täname.
                                        </div>
                                    </div>
                                    <div class="collapse" id="wrong5" data-parent="#accordion5">
                                        <div class="card card-body">
                                            Sinu vastus on väär. Proovi uuesti.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class = "modal-footer">
                                <button type = "button" class = "btn btn-default" data-dismiss = "modal">
                                    Tagasi
                                </button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div> <!-- col md-6 text center -->
        </div> <!-- /row -->
    </div> <!-- /tab panel show fade -->
    <div class="row" style="padding: 1.5em;">
        <ol>
            <li>Mina olen Issand, sinu Jumal. Sul ei tohi olla muid jumalaid minu kõrval.</li>
            <li>Sina ei pea mitte Issanda, oma Jumala nime ilmaasjata suhu võtma, sest Issand ei jäta seda nuhtlemata, kes Tema nime asjata suhu võtab.</li>
            <li>Sina pead pühapäeva pühitsema.</li>
            <li>Sina pead oma isa ja ema austama, et sinu käsi hästi käiks ja sina kaua elaksid maa peal.</li>
            <li>Sina ei tohi tappa.</li>
            <li>Sina ei tohi rikkuda abielu.</li>
            <li>Sina ei tohi varastada.</li>
            <li>Sina ei tohi anda valetunnistust oma ligimese vastu.</li>
            <li>Sina ei tohi himustada oma ligimese koda.</li>
            <li>Sina ei tohi himustada oma ligimese naist, sulast, ümmardajat, kariloomi ega midagi muud, mis talle kuulub.</li>
        </ol><br /><br />
        <p>Mida ütleb nüüd Jumal kõigi nende käskude kohta?</p>
        <p>Ta ütleb nii: Mina, Issand, sinu Jumal, olen vali Jumal, kes vanemate süü nuhtleb laste kätte kolmanda 
        ja neljanda põlveni neile, kes mind vihkavad, aga neile, kes mind armastavad ja minu käske täidavad, 
        osutan ma heldust tuhandest põlvest saadik.</p>
        <p>Jumal ähvardab nuhelda kõiki neid, kes astuvad üle neist käskudest. 
        Seepärast peame kartma Tema viha ega tohi teha nende käskude vastu, aga Tema tõotab armu 
        ja kõike head kõigile, kes neid käske täidavad. Seetõttu peamegi Jumalat armastama, Tema 
        peale lootma ja heal meelel Tema käskude järgi elama.</p>
    </div>
</div> <!-- card body-->
@endif
@endsection



