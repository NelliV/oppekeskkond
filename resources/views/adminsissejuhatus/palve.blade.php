@extends('layouts.app')

@section('content')
<!-- PALVE --> 
    @if(Auth::check())
    <div class="card-body shadow ">
        <div class="tab-pane show fade">
            <div class="row" style="padding: 1.5em;">
                <div class="col-md-6 embed-responsive embed-responsive-16by9" >
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ux-JVuEYkpY"  allowfullscreen></iframe>
                </div>
                <div class="col-md-6 text-center" >
                    <a style="margin: 2em; background-color:rgba(0,0,0,.09);"  data-toggle="modal" data-target="#test" class="btn btn-outline-success" href="test">Testi ennast!</a>
                    <!-- Siin on Palve test -->
                    <div class = "modal fade" id = "test" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
                        <div class = "modal-dialog">
                            <div class = "modal-content">
                                <div class = "modal-header">
                                    <h4 class = "modal-title" id = "myModalLabel">
                                        Meie isa palve
                                    </h4>
                                    <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                        &times;
                                    </button>
                                </div>
                                <div class = "modal-body">            
                                    <label>Meie Isa, kes Sa oled taevas</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right1" role="button">
                                            Seletus
                                        </a>
                                    </p>
                                    <div id="accordion1">
                                        <div class="collapse" id="right1" data-parent="#accordion1">
                                            <div class="card card-body">
                                            Uskuda tähendab Jumala Sõna kogu südamega vastu võtta ja Talle kõiges kuulekas olla.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>pühitsetud olgu Sinu nimi</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right2" role="button">
                                            Seletus
                                        </a>
                                    </p>
                                    <div id="accordion2">
                                        <div class="collapse" id="right2" data-parent="#accordion2">
                                            <div class="card card-body">
                                            Jumala nimi on küll iseenesest püha, kuid oluline on, et me seda 
                                            iga päev ka pühana suudame hoida ja pühitseda Jumala nime kogu oma tegevusega.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>Sinu riik tulgu</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right3" role="button">
                                            Seletus
                                        </a>
                                    </p>
                                    <div id="accordion3">
                                        <div class="collapse" id="right3" data-parent="#accordion3">
                                            <div class="card card-body">
                                            Iga inimesel on Jumala riigist oma ettekujutus. Uue Testamendi kohaselt on Jumala riik 
                                            Jeesuse Kristuse isikuga maa peale tulnud. Jeesus mõtleb siin Jumala riigi all inimeste 
                                            keskel elavat ja toimivat Jumalat. Seal, kus on Jumal, on ka Jumala riik. Ometi õpetab 
                                            Jeesus, et me paluksime, et Jumala riik tuleks. Siinkohal tähendab see Jeesuse taastulemist, 
                                            mille kohta Jeesus ütles, et selle aeg ei ole meile teada ja ainult Jumal teab seda.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>Sinu tahtmine sündigu nagu taevas nõnda ka maa peal</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right4" role="button">
                                            Seletus
                                        </a>
                                    </p>
                                    <div id="accordion4">
                                        <div class="collapse" id="right4" data-parent="#accordion4">
                                            <div class="card card-body">
                                            See on võib olla kristlaste jaoks kõige raskem palve. Keeruline on lasta sündida kellegi teise tahtmisel, 
                                            jättes enda soovid tahaplaanile. Selles palves me saamegi paluda, et Jumal annaks meile jõudu tema tahtmist täide viia.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>Meie igapäevast leiba anna meile tänapäev</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right5" role="button">
                                            Seletus
                                        </a>
                                    </p>
                                    <div id="accordion5">
                                        <div class="collapse" id="right5" data-parent="#accordion5">
                                            <div class="card card-body">
                                            Selles palves palume, et meil oleks kõike eluks hädavajalikku - toidust, kehakatet ja peavarju. Siin me 
                                            tunnustame, et kõik saadav ja saadu on kingitus ning et me tahame seda kingitust tänuga vastu võtta.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>ja anna meile andeks meie võlad nagu meiegi andeks anname oma võlglastele</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right6" role="button">
                                            Seletus
                                        </a>
                                    </p>
                                    <div id="accordion6">
                                        <div class="collapse" id="right6" data-parent="#accordion6">
                                            <div class="card card-body">
                                            Selles palves me tunnistame oma eksimusi ja palume, 
                                            et Jumal annaks Kristuse pärast need andeks ja võtaks meie palvet kuulda.
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>ja ära saada meid kiusatusse,vaid päästa meid ära kurjast,</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right7" role="button">
                                            Seletus
                                        </a>
                                    </p>
                                    <div id="accordion7">
                                        <div class="collapse" id="right7" data-parent="#accordion7">
                                            <div class="card card-body">
                                            Seitsmenda palve eesmärk on hoida meid eemale halvast ja samuti paluda, 
                                            et Jumal võtaks osa meie koormast enda kanda, kui me kiusatustes ja raskustes oleme.
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>sest Sinu päralt on riik ja vägi ja au igavesti. Aamen</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right8" role="button">
                                            Seletus
                                        </a>
                                    </p>
                                    <div id="accordion8">
                                        <div class="collapse" id="right8" data-parent="#accordion8">
                                            <div class="card card-body">
                                            Palve lõpetame ülistades. Selle kaudu on meil võimalik 
                                            oma usku tunnistada ja loota, et meie palvet kuulda võetakse.
                                            </div>
                                        </div>
                                    </div>
                                    <a href="http://www.eelk.ee/~ltund/voldikud/palve.html">Allikas</a>
                               </div>
                                <div class = "modal-footer">
                                    <button type = "button" class = "btn btn-default" data-dismiss = "modal">
                                        Tagasi
                                    </button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </div>
            </div>
        </div>
        <div class="row" style="padding: 1.5em;">
            <h3>Kuidas peab pereisa oma peret õpetama hommikuti ja õhtuti end õnnistama</h3>
            <p>Hommikul, kui sa voodist tõused, õnnista end püha ristimärgiga ja ütle:</p>
            <p>Jumala Isa, Poja ja Püha Vaimu nimel, Aamen.</p>
            <p>Seepeale loe põlvitades või seistes usutunnistus ja Meie Isa palve; ja kui sa tahad, 
            siis sa võid lisaks lugeda ka lühikese palve:</p>
            <p>Ma tänan Sind, minu taevane Isa, Jeesuse Kristuse, Sinu armsa Poja läbi, et Sa mind 
            sel ööl kõige kahju ja ohtude eest oled hoidnud, ja palun Sind, et sa hoiaksid mind 
            ka sel päeval patu ja kõige kurja eest, nii et minu teod ja kogu mu elu oleksid Sulle 
            meelepärased; sest ma usaldan end, oma ihu ja hinge ning kõik Sinu kätesse. Sinu püha 
            ingel olgu minuga, et kuri vaenlane minu üle võimust ei võtaks, Aamen.</p>
            <p>Ja seejärel mine rõõmuga oma tööde juurde ja laula näiteks mõni laul, näiteks  
            Kümne käsu kohta või midagi muud, mis su süda soovib.</p>
            <p>Õhtul, kui sa voodisse lähed, õnnista end püha ristimärgiga ja ütle:</p>
            <p>Jumala Isa, Poja ja Püha Vaimu nimel, Aamen.</p>
            <p>Seepeale loe põlvitades või seistes usutunnistus ja Meie Isa palve; ja kui sa tahad, 
            siis sa võid lisaks lugeda ka lühikese palve:</p>
            <p>Ma tänan Sind, minu taevane Isa, Jeesuse Kristuse, Sinu armsa Poja läbi, 
            et sa oled mind sel päeval armulikult hoidnud, ja ma palun sind, et sa 
            annaksid mulle andeks kõik minu patud, kus ma olen ülekohut teinud, ja hoiaksid mind sel 
            ööl armulikult; sest ma usaldan end, oma ihu ja hinge ning kõik sinu kätesse. 
            Sinu püha ingel olgu minuga, et kuri vaenlane minu üle võimust ei võtaks, Aamen.</p>
            <p>Ja seejärel jää kiiresti ja rõõmsalt magama.</p>
            
            <h3>Kuidas peab pereisa oma peret õpetama toidule õnnistust paluma ja selle eest tänama</h3>
            <p>Lapsed ja teenijad peavad ristitatud kätega ning kombekalt laua äärde astuma ja ütlema:</p>
            <p>"Kõikide silmad ootavad sind ja sina annad neile nende toidu parajal ajal. Sa avad oma 
            käe ja oma heameelega kosutad kõike elusat." [Ps 145:15j]</p>
            <p>Märkus:</p>
            <p>"Heameelega" tähendab seda, et kõik loomad saavad nii palju süüa, et nad on selle üle 
            rõõmsad ja heatujulised; sest mure ja ihnsus on sellisele heameelele takistuseks.</p>
            <p>Seejärel Meie Isa palve ja järgmine palve:</p>
            <p>ISSAND Jumal, taevane Isa, õnnista meid ja neid Sinu ande, mida me Sinu lahkest 
            heldusest vastu võtame, Jeesuse Kristuse, meie ISSANDA läbi, Aamen.</p>
            <h3>Tänamine</h3>
            <p>Samamoodi peavad nad ka pärast sööki kombekalt ja ristitatud kätega ütlema:</p>
            <p>"Tänage ISSANDAT, sest tema on hea! Tõesti, tema heldus kestab igavesti [Ps 106:1], 
            teda, kes annab leiva kõigele lihale [Ps 136:25], ta annab lojustele nende toidu, 
            kaarnapoegadele, kes teda hüüavad. Hobuse vägevusest ei pea ta lugu ega ole tal hea meel 
            jalameeste säärtest. ISSANDAL on heameel neist, kes teda kardavad, kes ootavad tema heldust 
            [Ps 147:9−11]."</p>
            <p>Seejärel Meie Isa palve ja järgmine palve:</p>
            <p>Me täname Sind, ISSAND Jumal Isa, Jeesuse Kristuse, meie ISSANDA läbi kõigi sinu heategude 
            eest, kes sa elad ja valitsed igavesti, Aamen.</p>
        </div>
    </div>
    @endif
@endsection



