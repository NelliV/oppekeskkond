@extends('layouts.app')

@section('content')
<!-- USUTUNNISTUS--> 
    @if(Auth::check())
    <div class="card-body shadow">
        <div class="tab-pane show fade">
            <div class="row" style="padding: 1.5em;">
                <div class="col-md-6 embed-responsive embed-responsive-16by9" >
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/AFJKB81N_po"  allowfullscreen></iframe>
                </div>
                <div class="col-md-6 text-center" >
                    <a style="margin: 2em; background-color:rgba(0,0,0,.09);"  data-toggle="modal" data-target="#test" class="btn btn-outline-success" href="test">Testi ennast!</a>
                    <!-- Siin on Usutunnistus test -->
                    <div class = "modal fade" id = "test" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
                        <div class = "modal-dialog">
                            <div class = "modal-content">
                                <div class = "modal-header">
                                    <h4 class = "modal-title" id = "myModalLabel">
                                        Usutunnistus test
                                    </h4>
                                    <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                        &times;
                                    </button>
                                </div>
                                <div class = "modal-body">            
                                    <label>Mina usun</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right1" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion1">
                                        <div class="collapse" id="right1" data-parent="#accordion1">
                                            <div class="card card-body">
                                            Uskuda tähendab Jumala Sõna kogu südamega vastu võtta ja Talle kõiges kuulekas olla.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>Jumalasse, kõigeväelisse Isasse, taeva ja maa Loojasse.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right2" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion2">
                                        <div class="collapse" id="right2" data-parent="#accordion2">
                                            <div class="card card-body">
                                            Jumal on täiuslik. Ta on Isa ja Poeg ja Püha Vaim. Armastusest on 
                                            Ta loonud kõik olemasoleva. Ta lõi kõik eimillestki ja hoiab kõike oma kätes. Isegi patt ega 
                                            kurjus ei suuda Tema tahet nurjata. Me võime Teda nimetada Isaks. Ta on halastuse Isa ja kõige 
                                            troosti Jumal, nagu ütleb Piibel.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>Ja Jeesusesse Kristusesse, Tema ainsasse Pojasse, meie Issandasse</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right3" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion3">
                                        <div class="collapse" id="right3" data-parent="#accordion3">
                                            <div class="card card-body">
                                            Kristlane usub ja tunnistab: Jeesus Naatsaretist on Kristus, elusa 
                                            Jumala Poeg, tõeline Jumal ja tõeline Inimene, vahemees Jumala ja inimeste vahel.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>Kes on saadud Pühast Vaimust, ilmale tulnud Neitsist Maarjast.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right4" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion4">
                                        <div class="collapse" id="right4" data-parent="#accordion4">
                                            <div class="card card-body">
                                            Kõigi inimeste pärast on igavese Isa igavene Poeg saanud inimeseks. Ilma inimliku 
                                            eostamiseta, Jumala väest võttis Neitsi Maarja Ta vastu ja tõi ilmale. Seepärast tunnustab ja 
                                            austab Kirik Neitsi Maarjat kui Jumalaema.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>Kannatanud Pontius Pilatuse ajal, risti löödud, surnud ja maha maetud.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right5" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion5">
                                        <div class="collapse" id="right5" data-parent="#accordion5">
                                            <div class="card card-body">
                                            Süütuna surma mõistetud, andis Jeesus Kristus meie eest vabatahtlikult 
                                            elu kui Jumala Tall, sõnakuulelik kuni surmani, pealegi ristisurmani. Nõnda heastas 
                                            Ta meie süü ja lepitas maailma Jumalaga.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>Alla läinud surmavalda, kolmandal päeval üles tõusnud surnuist.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right6" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion6">
                                        <div class="collapse" id="right6" data-parent="#accordion6">
                                            <div class="card card-body">
                                            Ka endiste aegade õiged, kes Kristuse tulemist alles ootasid, pääsesid Tema läbi
                                            surmavallast Jumala kirkusesse. Võitjana patu ja surma üle tõusis Ta hauast ja elab.
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>Üles läinud taeva, istub Jumala, oma kõigeväelise Isa paremal käel.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right7" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion7">
                                        <div class="collapse" id="right7" data-parent="#accordion7">
                                            <div class="card card-body">
                                            Isa andis Kristusele, meie Issandale ja Õnnistegijale, kogu meelevalla taevas ja maa peal. 
                                            Kuigi veel mitte kõik inimesed ei kuulu Tema riiki ja kuigi see pole veel saanud lõplikku 
                                            võitu, on see siiski juba praegu meie keskel.
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>Sealt Tema tuleb kohut mõistma elavate ja surnute üle.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right8" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion8">
                                        <div class="collapse" id="right8" data-parent="#accordion8">
                                            <div class="card card-body">
                                            Kui Kristus kohtumõistjana tagasi tuleb, kaob praegune maailm. Kõikide mõtted ja teod 
                                            saavad avalikuks ja Tema ees jääb püsima vaid headuses, õigluses ja armastuses tehtu.
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>Mina usun Pühasse Vaimusse.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right9" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion9">
                                        <div class="collapse" id="right9" data-parent="#accordion9">
                                            <div class="card card-body">
                                            Koos Isa ja Pojaga on Püha Vaim üks Jumal ja Issand, kelle Kristus on meile tõotanud ja 
                                            läkitanud kui Trööstija, kes jääb alati meie juurde. Vaid Tema väes suudame elada 
                                            kristlastena.
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>Üht püha kristlikku (üleilmset) Kirikut.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right10" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion10">
                                        <div class="collapse" id="right10" data-parent="#accordion10">
                                            <div class="card card-body">
                                            Jumal tahab, et kõik inimesed õndsaks saaksid ja tõe tundmisele jõuaksid – Kristuse 
                                            juurde, kes on Tõde. Tema Kirik, tõe sammas ja vundament, täidab Jumala päästetahet, 
                                            kuulutab ja vahendab Tema armu.
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>Pühade osadust</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right11" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion11">
                                        <div class="collapse" id="right11" data-parent="#accordion11">
                                            <div class="card card-body">
                                            Kirik koosneb inimestest, kes on küll patused, kuid Kristuses õigeks mõistetud ja Vaimus 
                                            pühitsetud. Kirikus on ühendatud nii need kristlased, kes rändavad veel maist teed, kui ka 
                                            pühakud taevas. Neid kõiki ühendab omavahel ainus jääv side – armastus.
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>Pattude andeksandmist</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right12" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion12">
                                        <div class="collapse" id="right12" data-parent="#accordion12">
                                            <div class="card card-body">
                                            Kristuse – Ristilöödu – ohvri ja võidu vili, Ülestõusnu ülestõusmisand on pattude 
                                            andeksandmine. Kirik vahendab seda neile, kes on valmis pöörduma ja meelt parandama
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>Ihu ülestõusmist</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right13" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion13">
                                        <div class="collapse" id="right13" data-parent="#accordion13">
                                            <div class="card card-body">
                                            Nagu Kristus tõusis üles ihulikult, nii tõuseme ka meie kord ja saame uue, vaimuliku, 
                                            kirgastatud ihu. Kristus ütleb: “Mina olen ülestõusmine ja elu. Kes minusse usub, see elab, isegi 
                                            kui ta sureb!”
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>Ja igavest elu</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right14" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion14">
                                        <div class="collapse" id="right14" data-parent="#accordion14">
                                            <div class="card card-body">
                                            Tõotus, et sellel, kes usub Pojasse, on igavene elu, ei hakka kehtima mitte alles 
                                            viimselpäeval ülestõusmises, vaid juba praegu – ristimise ja usu kaudu Kristusesse. Ta on 
                                            öelnud: “Igavene elu on aga see, et nad mõistaksid Sind, ainsat tõelist Jumalat, ja Jeesust Kristust, 
                                            kelle Sina oled läkitanud.”
                                            </div>
                                        </div>
                                    </div>
                                    <a href="http://www.eliisabet.ee/pdf/usutunnistus.pdf">Allikas</a>
                               </div>
                                <div class = "modal-footer">
                                    <button type = "button" class = "btn btn-default" data-dismiss = "modal">
                                        Tagasi
                                    </button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </div>
            </div>
        </div>
        <div class="row" style="padding: 1.5em;">
            <p>Mina usun Jumalasse, kõigeväelisse Isasse, taeva ja maa Loojasse.</p>
            <p>Ja Jeesusesse Kristusesse, Tema ainsasse Pojasse, meie Issandasse, 
            kes on saadud Pühast Vaimust, ilmale tulnud neitsi Maarjast, kannatanud 
            Pontius Pilaatuse all, risti löödud, surnud ja maha maetud, alla läinud surmavalda, 
            kolmandal päeval üles tõusnud surnuist, üles läinud taeva, istub Jumala, oma kõigeväelise 
            Isa paremal käel, sealt Tema tuleb kohut mõistma elavate ja surnute üle.</p>
            <p>Mina usun Pühasse Vaimusse, üht püha kristlikku Kirikut, pühade osadust, pattude andeksandmist, 
            ihu ülestõusmist ja igavest elu. Aamen.</p>
        </div>
    </div>
    @endif
@endsection



