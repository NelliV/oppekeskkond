@extends('layouts.app')

@section('content')
<!-- PIIBEL --> 
    @if(Auth::check())
    <div class="card-body shadow">
        <div class="tab-pane show fade">
            <div class="row" style="padding: 1.5em;">
                <div class="col-md-6 embed-responsive embed-responsive-16by9" >
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/hbRBpPS981Q"  allowfullscreen></iframe>
                </div>
                <div class="col-md-6 text-center" >
                    <a style="margin: 2em; background-color:rgba(0,0,0,.09);"  data-toggle="modal" data-target="#test" class="btn btn-outline-success" href="test">Testi ennast!</a>
                    <!-- Siin on Piibel test -->
                    <div class = "modal fade" id = "test" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
                        <div class = "modal-dialog">
                            <div class = "modal-content">
                                <div class = "modal-header">
                                    <h4 class = "modal-title" id = "myModalLabel">
                                        Piibel test
                                    </h4>
                                    <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                        &times;
                                    </button>
                                </div>
                                <div class = "modal-body">            
                                    <label>Issand on mu ...., mul pole millestki puudust.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right1" role="button">
                                            Jumal
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong1" role="button">
                                            karjane
                                        </a>
                                    </p>
                                    <div id="accordion1">
                                        <div class="collapse" id="right1" data-parent="#accordion1">
                                            <div class="card card-body">
                                                Vale. Proovi uuesti
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong1" data-parent="#accordion1">
                                            <div class="card card-body">
                                                Õige - Ps 23:1
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>2. Ka kui ma kõnniksin pimedas orus, ei karda ma ..., 
                                        sest sina oled minuga; su karjasekepp ja su sau, 
                                        need trööstivad mind.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right2" role="button">
                                            pimedust
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong2" role="button">
                                            kurja
                                        </a>
                                    </p>
                                    <div id="accordion2">
                                        <div class="collapse" id="right2" data-parent="#accordion2">
                                            <div class="card card-body">
                                                Vale vastus. Proovi uuesti.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong2" data-parent="#accordion2">
                                            <div class="card card-body">
                                                Õige - Ps 23:4
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>3. Sinu sõna on mu jalale lambiks ja ... mu teerajal.</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right3" role="button">
                                            karguks
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong3" role="button">
                                            valguseks
                                        </a>
                                    </p>
                                    <div id="accordion3">
                                        <div class="collapse" id="right3" data-parent="#accordion3">
                                            <div class="card card-body">
                                                Vale vastus. Proovi uuesti!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong3" data-parent="#accordion3">
                                            <div class="card card-body">
                                                Õige - Ps 119:105
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>4. Igale asjale on määratud ..., ja aeg on igal tegevusel 
                                        taeva all</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right4" role="button">
                                            aeg
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong4" role="button">
                                            tegevus
                                        </a>
                                    </p>
                                    <div id="accordion4">
                                        <div class="collapse" id="right4" data-parent="#accordion4">
                                            <div class="card card-body">
                                                Õige - Kg 3:3
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong4" data-parent="#accordion4">
                                            <div class="card card-body">
                                                Vale. Proovi uuesti.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>5. Hõlpsam on ... minna läbi nõelasilma kui rikkal minna Jumala riiki!</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right5" role="button">
                                            hundil
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong5" role="button">
                                            kaamelil
                                        </a>
                                    </p>
                                    <div id="accordion5">
                                        <div class="collapse" id="right5" data-parent="#accordion5">
                                            <div class="card card-body">
                                                Vale vastus. Proovi uuesti.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong5" data-parent="#accordion5">
                                            <div class="card card-body">
                                                Õige - Matteuse 19:24, Markuse 10:25, Luuka 18:25
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-footer">
                                    <button type = "button" class = "btn btn-default" data-dismiss = "modal">
                                        Tagasi
                                    </button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </div>
            </div>
        </div>
        <div class="row" style="padding: 1.5em;">
            <p>Piibel on kristliku usu alus. Erinevalt kõikidest teistest raamatutest, kus inimene kõneleb 
            inimesega, on Piibel raamat, kus  Jumal kõneleb inimesega ja kus me õpime tundma Jumalat ennast, 
            tema tegusid ja tema tahet. Seega on Piibel eelkõige usuraamat. Sellepärast nimetatakse seda ka 
            Raamatute Raamatuks. Nimetus Piibel tuleneb kreeka keelse sõna  biblia= raamatud latiniseeritud 
            vormist ja on esimest korda kasutatud kirikuisa Christostomuse poolt.</p>
            
            <p>Piibli kirjutamine on antud ja usaldatud inimeste kätte. Sellepärast kannab see endas inimlikke 
            jooni  ja oma ajastu pitserit.</p>

            <p>Piibel on mitmekülgne raamat. Nii on  Piibel on ajalooraamat, mis kirjeldab kõige loomise 
            algust, ..... ja räägib viimaks sellest, kuidas maailma aeg kord lõpeb. Kristliku ajaloo 
            käsitlemise eripäraks on, et ajaloos nähakse Jumala toimimist läbi aegade. VT-s  
            näidatakse seda ühe rahva, Iisraeli rahva, ajaloo läbi. Selles kirjelduses on 
            näidatud inimesi ja rahvaid nende tõeluses, ilma neid ilustamata. Seepärast on Vana Testament 
            sagedasti karm, lausa jubedust tekitav  oma kirjeldustes.</p>
            
            <p>Piibel on see, mis seob  kristlasi kogu maailmas. Sadadel, tuhandetel kristlikel kirikutel 
            ja konfessioonidel on kõigil üks pühakiri, Piibel.</p>
            
            <p>Piibel on jääv ja muutumatu.  Jeesus ütleb:"Tõesti, ma ütlen teile, ükski täpp ja ükski 
            kriips ei kao Seadusest seni, kuni taevas ja maa püsivad, kuni kõik, mis sündima peab, on 
            sündinud!"  /Mt 5:18/</p>
    
            <p>Piibel jaguneb Uueks Testamendiks ja Vanaks Testamendiks. Siia juurde kuuluvad veel apokrüüfid. 
            Õigeusu-  katoliku kirikus on need arvatud kanooniliste raamatute hulka. Luterlikus kirikus leiame 
            neid ainult suuremate Piiblite väljaannetest. Nende kohta on öeldud, et neid on hea ja kasulik 
            lugeda aga need ei kuulu otseselt kanooniliste raamatute hulka. Apokrüüfe on veelgi võrratult enam, 
            kuid kõiki ei ole võetud Piiblisse.</p>

            <p>Sõna "testament" tuleneb ladina keelsest sõnast "testamentum", mis tähendab "tahet" või "lepingut". 
            Seega on siis Vana Testament vana lepingu raamat,  Jumala leping ja tahte väljendus Iisraeli 
            rahvale Aabrahami ja Moosese läbi. Uus testament on aga uue lepingu raamat, 
            Jumala leping kogu maailmaga, tema tahte väljendus maailmale oma Poja  Jeesuse Kristuse läbi. 
            Kui vana leping tugineb eelkõige käsule ja selle täitmata jätmisel järgnevatest tagajärgedest, 
            siis uus leping on armastusel ja vabadusel rajanev leping, kus eesõigus on armul ja halastusel.</p>

            <p>Vana Testament räägib ajast enne Kristuse sündi. Ajalugu tuuakse meie ette iisraeli rahva 
            näite varal, kes on  Jumalast eriliselt valitud ja kutsutud Piibli ajaloo keskseks kandjaks. 
            Kui Piibli pärimus ulatub päris aegade algusesse, siis kirja on pandud VT raamatud ajavahemikus  
            IX - II saj. e.Kr.  Valdav osa raamatutest on kirjutatud heebrea keeles. Üksikud osad - 
            mõned sõnad ja peatükid - on kirjutatud  aramea keeles. Heebrea keelele väga lähedase 
            sugulaskeelena  oli  aramea keel rahva kõnekeeleks Jeesuse aegses Palestiinas. Nii kohtamegi 
            me ka Uues Testamendis mõningaid aramea keelseid väljendeid. Heebrea keeles kirjutatakse 
            vasakult paremale. Vanimad säilinud  tekstid on kirjutatud papüürose peale jätkuvas reas 
            ilma sõnavahedeta. Veel on vana heebrea keele eripäraks, et  kirjutati välja ainult kaashäälikud 
            jättes täishäälikud märkimata. Täishääliku märgid ja teksti liigendus  lisati alles 
            7-9 saj. p.Kr.  Kuid selleks ajaks oli nii mõnigi igivana asi juba ununenud. 
            Siia juurde võib ühel sõnatüvel olla palju erinevaid tähendusi. Siit tulenevadki 
            suured raskused heebrea algtekstide tõlkimisel.  Valdavas osas on jõutud  ühisele 
            äratundmisele, kuid üksikuid kohti on erinevates piiblitõlgetes tõlgitud ka erinevalt.</p>

            <p>Vana Testament koosneb 39 raamatust (ilma apokrüüfideta) ja  jaotub: </p>
            <ol>
                <li>Seadus (hbr.k. Toora) -  5 Moosese raamatut  e. Pentateuhh.</li>
                <li>Ajaloo raamatud - Joosua,  Kohtumõistjad, Saamueli raamatud, Kuningate raamatud, Ajaraamatud, 
                Esra, Nehemja</li>
                <li>Poeesia ja tarkuse kirjandus - Psalmid, Iiobi raamat, Õpetussõnad,Ülemlaul, Koguja, 
                Nutulaulud</li>
                <li>Prohvetite raamatud (Suured prohvetid ja Väikesed Prohvetid</li>
                <li>Pühad kirjutused -  Rutt, Ester, Taaniel</li>
                <li>Apokriiva raamatud - Juudit, Saalomoni tarkuseraamat, Toobit, Jeesus Siirak, 
                Baaruk, Makkabite raamatud, Estri raamatu lisad, Taanieli raamatu lisad</li>
            </ol>
            
            <p>Eriti vanemate ajalooliste sündmuste, enne kuningas Sauli, on täpse aja määramine väga raske, 
            erinevatel autoritel on erinevad andmed. Kuid ilmselt on siin meie jaoks pigem oluline  õige 
            ajastu  ja  sündmuste järjestus. </p>

            <p>Uus Testament räägib meile ajast alates Kristuse sünnist ja lõpeb maailma aja 
            otsasaamisega.</p>
            
            <p>Raamatud on kirjutatud ajavahemikus ca 50-95 p.Kr.</p>

            <p>Uus testament on algselt kirjutatud kreeka keeles suurte tähtedega nn. majuskilitega. 
            Siin kirjutati  algselt  tekst samuti ühes reas ilma sõnavahesid jätmata. Teksti lugemise 
            hõlbustamiseks on seda püütud  juba V sajandil Aleksandria diakoni Euthaliuse poolt liigendada. 
            Tänapäevane Piibli jaotamine peatükkideks toimus alles XIII sajandil, 
            jaotamine salmideks aastal 1548.</p>

            <p>Uus Testament koosneb 27 raamatust, mis jagunevad: evangeeliumid, apostlite 
            tegude raamat, apostlite kirjad, kiri heebrealastele ja Johannese ilmutuse raamat.</p>

            <p>Kui Piiblit lugeda pealiskaudselt, siis jääb meile sealt palju tähelepanemata ja mõistmata. 
            Piibli lugemist on võrreldud tähistaeva uurimisena. Tundub nagu oleks kõik teada ja läbiuuritud, 
            kuid ikka ja taas avastatakse siin uusi nähtusi ja tähti. Mida võimsam ja puhtam on teleskoop, 
            seda suuremad on väljavaated, midagi uut avastada; mida usukindlam ja puhtam on süda, seda enam 
            on meil võimalusi leida uut usumaailmas. Lugemist mõistmise tasandid- ajalooline, vaimne, 
            allegooriline...</p>

            <p>Luterliku kiriku üks rõhuasetus on selles, et inimene ise vahetult suhtleks Jumalaga. 
            Oluline koht selles on, et Pühakiri oleks kõikide jaoks kättesaadav ja mõistetav. 
            Varasemas ajas oli katoliku kirikus tavaks, et Piibel oli kasutusel  ainult 
            ladina keeles. Koguduse liikmetele anti lugemiseks Piibli tervikust palveraamatutes 
            ainult väljavalitud üksikud osad.  Kuni IV sajandini olid Piibli raamatud  oma 
            välises kujus rullikeeratud papüüruse- või naharibad. Keskmiselt oli rulli laiuseks 
            12 - 35 cm ja pikkuseks 6   meetrit ; aga  on olemas ka papüürusrull pikkusega 10.5 - 12.3 -  
            46.5 meetrit. Tekst oli tavaliselt kirjutatud ühele poole 6.4 - 7.6cm laiuste tulpadena. 
            Papüüruse suureks puuduseks on, et see ei kannata ei  niiskust ega ka liigset kuiva. 
            Niiskusega papüürus pehkib, kuivuses murdub ja pudeneb.  Seetõttu on papüürusele kirjutatud 
            Piibli   käsikirjade säilinud arv väga väike.  Säilinud on fragmente, vanimad tervikkäsikirjad 
            on   4-5. sajandist. Juba varakult on UT kirju tõlgitud - kreeka keelne Septuaginta  (LXX)  
            3-1 saj. e.Kr. , Süüria Peśittta  3.saj., ladina keelne Vulgata 4 saj. , 9-15 saj.  
            on Vulgata alusel tehtud suur hulk rahvakeelseid tõlkeid. </p>

            <p>IV sajandil sai normiks raamatu  tänapäevane kuju, samaaegselt tõrjus pärgament 
            välja papüüruse. Raamatuid kirjutati munkade poolt käsitsi . Sõltuvalt käsikirja mahukusest, 
            kirja keerukusest ja illustratsioonide olemasolust võis selleks kuluda kuid või isegi aastaid. 
            Nii oli raamat väga kallis. Näiteks maksti X sajandil ühe jutlusekogu eest 200 lammast, 3 tünni 
            vilja ja lisaks veel nugisenahku; 1074.a. on saadud ühe missaali e. kirikuteenistusraamatu eest 
            tasuks terve viinamägi.  Columba, Śotimaa misjönär VI sajandil,  laseb oma õpilastel   
            kõik Psalmid pähe õppida, mõnedel on ka kogu UT peas.  Ka Lutheri ajal ei olnud veel 
            sugugi igal ülikoolil, kloostril või kirikul Piiblit terviklikult; nii mõnelgi  olid 
            vaid tähtsamad  raamatud üldkogumist.  Alles tänu paberi üldisele kasutuselevõtmisele XV 
            sajandist,  trükikunsti leiutamisele 1456.a.   ja  Piibli tõlkimisele rahvuskeeltesse saab 
            Piibel rahvaraamatuks.  Apokriivade  kohta on Luther öelnud, et kuigi see ei kuulu otseselt 
            Pühakirja hulka on seda siiski kasulik ja hea lugeda.</p>

            <p>Piiblis kui raamatus  on  osa Jumala aust ja pühadusest, seepärast öeldaksegi Piibli 
            kohta  Pühakiri. Sellisena  väärib see kohtlemisel erilist austust ja lugupidamist. 
            Näiteks ei panda kunagi teisi raamatuid Piibli  peale.</p>
            
            <a href="http://www.eelk.ee/voru.katariina/">Allikas</a>
        </div>
    </div>
    @endif
@endsection



