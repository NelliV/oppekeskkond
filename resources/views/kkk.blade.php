@extends('layouts.app')

@section('content')

<!--@if(Auth::check()) -->
<div class="card-header card bg-success text-white">Korduma Kippuvad Küsimused</div>
<div class="card-body shadow">
    <div id="accordion">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link text-success" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Kas Jeesus on tõesti Jumal?
                    </button>
                </h5>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <p>Jumal võis saada inimeseks samal põhjusel nagu Jeesus tegi imesid ja Jumal võis luua 
                    maailma mitte millestki – nimelt saab Jumal teha nii, nagu Ta parasjagu tahab. 
                    Jumal võib teha kõike, mis pole vastuolus Tema loomusega. "Inimeste käes on see võimatu,
                    kõik on aga võimalik Jumala käes." (Matteuse 19:26)</p>
                    <p>Aastaid enne, kui Issand tuli Naatsareti Jeesusena, teatas Ta oma tulekust 
                    (Vana Testamendi prohveteeringud). Ning kui Ta saabus, ütles Ta meile täpselt, kes Ta on. 
                    Näiteks: "Enne kui Aabraham sündis, olen mina." (Johannese 8:58), "Mina ja Isa oleme üks." 
                    (Johannese 10:30) ja "Kes on näinud mind, see on näinud Isa." (Johannese 14:9). 
                    Selle tõestuseks tegi Jeesus imesid, mida ükski inimene ei ole teinud ning mille 
                    kulminatsioon oli Tema ülestõusmine.</p>
                    
                    <p>Me mõtleme Jeesusest kui nähtamatu Jumala nähtavast kujust. Ta võttis selle kuju, 
                    et tuua meieni Tema armastus ja näidata meile teed Temani, ning viisi, kuidas 
                    elada Tema jaoks. Seeläbi Ta ka tagas meile tee Tema juurde. Jeesus tegi seda, 
                    lubades end surmata – risti lüüa – andestuseks meie pattude pärast. 
                    Piibel ütleb, et Jeesus „kes olles Jumala kuju, ei arvanud osaks olla Jumalaga 
                    võrdne, vaid loobus iseenese olust, võttes orja kuju, saades inimese sarnaseks; 
                    ja ta leiti välimuselt inimesena. Ta alandas iseennast, saades kuulekaks surmani, 
                    pealegi ristisurmani.” (Filiplaste 2:6–8).</p>
                    
                    <p>Jeesuses sai üleloomulik ja kogemusväline Jumal "käegakatsutavaks", et surra meie 
                    eest saades meie jaoks sillaks Temani, Jumalani. Veel sellest, kuidas Piibel Jumala 
                    inimeseks saamist seletab:</p>
                    
                    <p>"Alguses oli Sõna ja Sõna oli Jumala juures ja Sõna oli Jumal. […] Ja Sõna sai lihaks 
                    ja elas meie keskel." (Johannese 1:1; 14)</p>
                    <p>"Tema [Jeesus Kristus] on nähtamatu Jumala kuju." (Koloslaste 1:15)</p>
                    <p>"Sest temas elab kogu Jumalik täius ihulikult.” (Koloslaste 2:9)</p>
                    <p>Heebrealaste kirja alguses öeldakse, et Jumala Poeg on "tema kirkuse kiirgus ja 
                    tema olemuse kuju" (Heebrealaste 1:3).</p>
                    <a href="https://www.tudengielu.net/a/kasjeesus.html">Allikas</a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                    <button class="btn btn-link text-success collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Kas Jeesus on ainus tee Jumala juurde?
                    </button>
                </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    <p>Jeesus ütles, et Tema on Tee, Tõde ja Elu (Johannese 14:6). Ta kas rääkis tõtt või mitte. Kui ta rääkis tõtt, 
                    siis pole naiivne Temaga nõustuda.</p>
                    <p>Üldiselt arvatakse, et Kristuse järgijad on pärit Läänest. Kuid Jeesus alustas Kesk-Idas ning Tema sõna levis 
                    sealt edasi üle kogu maailma. Aafrikas ja Aasias on palju usklikke. Hetkel on suurim kristlik kirik maailmas 
                    Lõuna-Koreas.</p>
                    <p>Usk Jeesusesse ei ole ainult Lääne "asi", kuigi skeptikud ja vastased üritavad sellist pilti maalida. 
                    Piibel ütleb, et suur hulk usklikke, kes saavad olema taevas, on pärit kõigist rahvahõimudest ja suguharudest 
                    ja rahvaist ja keeltest (Ilmutuse 7:9), ja et Jeesus suri kõigist suguharudest, rahvustest, riikidest ja 
                    keeltest pärit inimeste pattude eest.</p>
                    <p>Kui Jeesus on tõesti Jumal, nagu ta ütles, ja tõestuseks tõusis üles surnuist, siis on Ta meile andnud 
                    rohkelt põhjuseid Temasse uskumiseks. Ta ei nõua meilt naiivset vaid teadlikku usku Temasse. Need, kes usuvad 
                    Jeesusesse, saavad rikkama elu, oma elule suuna, rahu Jumalas ja igavese elu. Vaevalt, et see on uisa-päisa tehtud 
                    otsus.</p>
                    <a href="https://www.tudengielu.net/a/kasjeesus.html">Allikas</a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                    <button class="btn btn-link text-success collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Aga kõik see kurjus, mida tehakse Jumala nimel?
                    </button>
                </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    <p>Inimesed teevad igasugu asju Jumala ja Jeesuse nimel, asju, milleks Jeesus meid ei kohustanud 
                    ning mida Ta meil teha ei palunud. Inimesed teevad seda, mida nad tahavad, ja seejärel püüavad leida oma 
                    teguviisidele õigustust.<p>
                    <p>Jumal teab alati, kellele ja mis ülesanded Ta andnud on või ei ole. Lõpuks saab avalikuks, 
                    mida on maailmas tehtud Jumala tahte kohaselt ning mis on olnud inimeste endi tahe, ahnus ja kurjus.</p>
                    <p>Inimeste tegusid ei saa tuua argumentideks Jumala eitamisel. Need ei tee Jumalat olematuks. 
                    Jumal on hea, täiuslik, õiglane, halastav, armastav ja aus. Kui keegi tegutseb Tema tahte vastaselt, 
                    ent Tema nimel, ei saa me selles süüdistada Jumalat. Ja Ta ka ei luba meil seda teha.</p>
                    <p>Lõpuks peab iga inimene Jumala ees oma tegude kohta aru andma ning sel hetkel ei ole enam ruumi 
                    vabandustele ja eneseõigustamisele. Me kas otsisime oma elus Jumalat ja lubasime Temal meid muuta või mitte.</p>
                    <p>Jumal otsustab igaühe üle õiglaselt ning mõistab kohut vastavalt inimese südame kavatsustele ja motiividele.</p>
                    <p><strong>Piiblist:</strong></p>
                    <p>"Kõik mehe teed on ta enese silmis õiged, aga Issand katsub südamed läbi." (Õpetussõnad 21:2)</p>
                    <p>"Issand katsub läbi õige, aga õelat ja vägivalla armastajat vihkab ta hing." (Psalmid 11:5)</p>
                    <p>Nõnda siis ärge mõistke kohut enne õiget aega, enne kui tuleb Issand, kes toob valguse ette 
                    pimedusse varjunud asjad ning teeb avalikuks inimsüdamete kavatsused; ja siis saab igaüks kiituse Jumalalt." 
                    (1. Korintlaste 4:5)</p>
                    <p>"Kuid Issand jääb igavesti; ta on oma aujärje seadnud kohtumõistmiseks." (Psalmid 9:8)</p>
                    <p>"Sest ta tuleb mõistma kohut maailmale; ta mõistab kohut maailmale õiguses ja rahvaile õigluses." (Psalmid 98:9)</p>
                    <p>"Vaid ta mõistab viletsaile kohut õiguses ja otsustab hädaliste asju maa peal õigluses; ta lööb oma suu vitsaga maad ja surmab oma huulte puhanguga õela." (Jesaja 11:4)</p>
                    <p>"Ja ta ütles neile: "Te olete need, kes end õigena näitavad inimeste silmis. Aga Jumal tunneb teie südant, sest mis inimeste seas on kõrge, see on Jumala ees jäledus."" (Luuka 16:15)</p>
                    <p>"Sest Jumala sõna on elav ja tõhus ja vahedam kui ükski kaheterane mõõk, ning tungib läbi, kuni ta eraldab hinge ja vaimu, liigesed ja üdi, ning on südame meelsuse ja kaalutluse hindaja." (Heebrealaste 4:12)</p>
                    <p>"Ja hävitan ta lapsed surmaga, ja kõik kogudused saavad aru, et mina olen see, kes uurib läbi meeled ja südamed, ning annan igaühele teist teie tegusid mööda.“ (Ilmutuse 2:23)</p>
                    <p>"Ma nägin suurt valget trooni ja seda, kes sellel istub, kelle palg eeest põgenesid maa ja taevas, ning neile ei leidunud aset. Ja ma nägin surnuid, suuri ja pisikesi, seisvat trooni ees, 
                    ning raamatud avati. Teine raamat avati, see on eluraamat. Ja surnute üle mõisteti kohut seda mööda, kuidas raamatuisse oli 
                    kirjutatud, nende tegude järgi. Ja meri andis tagasi oma surnud ning surm ja surmavald andsid tagasi oma surnud ning igaühe 
                    üle mõisteti kohut tema tegude järgi." (Ilmutuse 20:11-13)</p>
                    <p>Saamaks teada, kuidas saada suhet Jumalaga, vaata: Jumal on lähemal kui arvame.</p>
                    <a href="https://www.tudengielu.net/a/nimel.html">Allikas</a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingFour">
                <h5 class="mb-0">
                    <button class="btn btn-link text-success collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                    Kas abielu-eelne seks on vale?
                    </button>
                </h5>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                <div class="card-body">
                    <p>Vaadelgem näiteks seda: kas autojuhtimine on vale? Ei. Kas 13aastasena on autojuhtimine vale? Jah. 
                    13aastase lapse jaoks on see küll lõbus ja huvitav, kuid seab tema ja ka teiste elu ohtu.</p>
                    <p>Kas seks, mis on lõbus, on abikaasade vahel vale? Ei. Kas seks on vale, kui seda tehakse kellegi 
                    teise abikaasaga? Jah. See võib küll olla lõbus ning põnev, ent tihti toob see kaasa kohutava südamevalu 
                    selle inimese abikaasale ja lastele.</p>
                    <p>Inimestele meeldib määratleda head ja halba oma tahtmiste kohaselt. See on inimloomus. 
                    Kui me tahame kellegagi vahekorras olla, soovime seada enda standardid. Sageli on nad stiilis: "Kui 
                    ta ei ole abielus, siis võib temaga vahekorras olla." Kuid mis saab juhul, kui teine inimene on 
                    nakatunud mõne sugulisel teel leviva haigusega? Reeglid muutuvad häguseks. 
                    Või mis siis, kui ta jääb rasedaks ja on silmitsi valikuga, kas teha aborti või mitte? Samuti hägune. 
                    Mis juhtub, kui see inimene on sugulane? Mis siis, kui ta on samast soost? Mis siis, kui seksitakse raha pärast? 
                    Pornograafia pärast? Mis siis, kui sellesse kaasatakse lapsi?</p>
                    <p>Mis on lõbus ja huvitav ühele, võib kellegi teise seisukohalt olla väga vale. 
                    Kas pole? Kust läheb õige ja vale piir?</p>
                    <p>Armastav Jumal on meile teatavaks teinud, kuidas oleks õige elada. Ta ütleb, et patt pakub vaid hetkelist 
                    naudingut. Tõenäoliselt pole olemas pattu, mis ei ole sel hetkel meeldiv. 
                    Kuid nauditavus ei saa olla meile otsuste langetamisel ainsaks standardiks. 
                    Kujuta ette, kui hea oleks olnud oma nooremat õde või venda mõnikord korralikult klobida. 
                    Nauding hetkeks, aga õnneks me hoiame end tagasi, kuna see tunne ei ole meie ainus juhtija.</p>
                    <p>Jumal tahab meid hoida tohututest probleemidest, mida me võime endale põhjustada oma rumalate otsustega. 
                    Ta tõesti armastab meid ja tahab meid kaitsta otsuste ja käitumise eest, mis meie või kellegi teise elu hävitavad.</p>
                    <p>Miks piirab Jumal seksi abieluga? Kas selleks, et ära rikkuda inimeste rõõmu või hoopis kindlustada, 
                    et partnerid naudiksid sügavaimat intiimsust, mis oleks vaid neile kahele mõeldud? 
                    Kui Jumal meile juhiseid annab, on Tema motiivid puhtad ja ajendatud armastusest meie vastu.</p>
                    <p>Inimesed lubavad ennast seksuaalselt ära kasutada üksnes lühiajalise lõbu ja põnevuse pärast, 
                    aga mis siis, kui on olemas midagi palju väärtuslikumat kui hetkeline nauding? Näiteks väärikus, 
                    enesehinnang ja teadmine, et ka teine inimene on väärtuslik? Võib-olla on Jumal ette näinud, 
                    et suhe muutub intiimsemaks, turvalisemaks ja tugevamaks, kui see on ehitatud millelegi 
                    olulisemale kui seksuaalne side. Jumala tarkus ületab alati meie oma ning Teda saab usaldada, 
                    ükskõik, mis mõtted Tal ka poleks. Ja üsna tihti näeme me alles hiljem Temale järgnemise vilja.</p>
                    <a href="https://www.tudengielu.net/a/seks1.html">Allikas</a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingFive">
                <h5 class="mb-0">
                    <button class="btn btn-link text-success collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                    Milline on taevas? Kas taevas on tõesti olemas?
                    </button>
                </h5>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                <div class="card-body">
                    <p>Jah, taevas on tõepoolest olemas.</p>
                    <p>See, mida enamik inimesi kutsub "taevaks", on tegelikult igavene linn, mida piibel nimetab "uueks Jeruusalemmaks" 
                    (Johannese ilmutus 21:2). Igavest linna kirjeldatakse nii:</p>
                    <blockquote class="blockquote text-center">"Vaata, Jumala telk on inimeste juures ning tema asub nende juurde elama 
                    ning nemad saavad tema rahvaiks ning Jumal ise on nende juures nende Jumalaks. Tema pühib ära iga pisara nende silmist 
                    ning surma ei ole enam ega leinamist ega kisendamist, ning valu ei ole enam, sest endine on möödunud." Ja troonil istuja
                    ütles: "Vaata, ma teen kõik uueks!"
                    <footer class="blockquote-footer">(Johannese ilmutus 21:3-5)</footer>
                    </blockquote>
                    <p>Uus Jeruusalemm on Jumala rahva igavene kodu. Piibel ütleb selle koha kohta: "Sinna ei saa midagi, mis on rüve, 
                    ega keegi, kes teeb jäledusi ja valet, vaid üksnes need, kes on kirjutatud [Jeesuse] eluraamatusse" 
                    (Johannese ilmutus 21:27).</p>
                    <p>Seal öeldakse, et kõik inimesed kõik inimesed tõusevad ihulikult surnuist üles ja kõik ilmuvad Kristuse 
                    kohtujärje ette (Johannese ilmutuse 20:11-13). Ehkki Jeesus tuli algselt maa peale Päästjana, on Ta nüüd Kohtunik. 
                    Need, kelle nimesid ei leita olevat eluraamatusse kirjutatud, visatakse tulejärve (Johannese ilmutuse 20:14-15).</p>
                    
                    <p>Arusaam, et kõik lähevad taevasse ja näevad oma sugulasi, ei ole üldse piibellik. 
                    Jeesus ütles, et ainult need, kes Temasse usuvad, leiavad elu, sest "ükski ei saa minna Isa juurde muidu kui minu kaudu" 
                    (Johannese 14:6). Ilmutuse 7:9 öeldakse, et taevas on suur rahvahulk kõigist hõimudest ja suguharudest, 
                    keeltest ja rahvastest, kes saavad igavese elu, kuna nad usuvad Jeesusesse. 
                    Need, kes on Jumala tagasi lükanud, et saa olla koos Jumalaga.</p>
                    <a href="https://www.tudengielu.net/a/taevas.html">Allikas</a>
                </div>
            </div>
        </div>
    <!--<div class="card">
            <div class="card-header" id="headingSix">
            <h5 class="mb-0">
                <button class="btn btn-link text-success collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
                Korduma kippuv küsimus number 6
                </button>
            </h5>
            </div>
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
            <div class="card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingSeven">
            <h5 class="mb-0">
                <button class="btn btn-link text-success collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseThree">
                Korduma kippuv küsimus number 7
                </button>
            </h5>
            </div>
            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
            <div class="card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingEight">
            <h5 class="mb-0">
                <button class="btn btn-link text-success collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseThree">
                Korduma kippuv küsimus number 8
                </button>
            </h5>
            </div>
            <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
            <div class="card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingNine">
            <h5 class="mb-0">
                <button class="btn btn-link text-success collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseThree">
                Korduma kippuv küsimus number 9
                </button>
            </h5>
            </div>
            <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
            <div class="card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTen">
            <h5 class="mb-0">
                <button class="btn btn-link text-success collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseThree">
                Korduma kippuv küsimus number 10
                </button>
            </h5>
            </div>
            <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
            <div class="card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
            </div>
        </div>-->
    </div>  
</div>

<!--@endif-->

@endsection
