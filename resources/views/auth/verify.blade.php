@extends('layouts.app')

@section('content')
<div class="card-header">{{ __('Kinnita oma E-maili aadress') }}</div>

<div class="card-body">
    @if (session('resent'))
        <div class="alert alert-success" role="alert">
            {{ __('Kinnituslink on saadetud Teie E-mailie.') }}
        </div>
    @endif

    {{ __('Enne jätkamist kontrolli kinnituslinki oma e-mailis.') }}
    {{ __('Kui sa ei saanud emaili') }}, <a href="{{ route('verification.resend') }}">{{ __('vajuta siia uue saatmiseks') }}</a>.
</div>
@endsection
