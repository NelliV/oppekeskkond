<!-- edit.blade.php -->

@extends('layouts.app')

@section('content')

<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Muuda õpetus
    <div class="float-right">
        <a class="btn btn-success" href="{{ url('/varasalvs') }}">
            <i class="fa fa-backspace"></i>{{ __('  Tagasi') }}
        </a>
    </div>
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('varasalvs.update', $varasalv->id) }}">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="name">Kategooria :</label>
              <input type="text" class="form-control" name="kategooria" value="{{$varasalv->kategooria}}"/>
          </div>
          <div class="form-group">
              <label for="price">Pealkiri :</label>
              <input type="text" class="form-control" name="pealkiri" value="{{$varasalv->pealkiri}}"/>
          </div>
          <div class="form-group">
              <label for="quantity">Lühikirjeldus :</label>
              <input type="text" class="form-control" name="lyhikirjeldus" value="{{$varasalv->lyhikirjeldus}}"/>
          </div>
          <div class="form-group">
              <label for="quantity">Tekst :</label>
              <input id="mytextarea" type="textarea" class="form-control" name="tekst" value="{{$varasalv->tekst}}"/>
          </div>
          <button type="submit" class="btn btn-success">Uuenda õpetus</button>
      </form>
  </div>
</div>
@endsection