<!-- show.blade.php -->

@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    <div class="float-left"><h2>{{$varasalv->pealkiri}}</h2></div>
    <div class="float-right">
        <a class="btn btn-success" href="{{ (url()->previous()) }}">
            <i class="fa fa-backspace"></i>{{ __('  Tagasi') }}
        </a>
    </div>
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    <div class="form-group">
        @csrf
        <p>{!! $varasalv->tekst !!}</p>
    </div>
</div>
@endsection