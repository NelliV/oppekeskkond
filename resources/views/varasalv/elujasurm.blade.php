@extends('layouts.app')

    @section('content')

        <div class="card-header">
            <div class="float-left"><h2>Elu ja surm<h2></div>
            <div class="float-right">
                <a class="btn btn-success" href="{{ url('/varasalv') }}">
                    <i class="fa fa-backspace"></i>{{ __('  Varasalv') }}
                </a>
            </div>
        </div>

        <div class="card-body">
            <div class="container-fluid">
                @foreach($data as $item)
                <div class="row top-buffer">
                    <div class="col-md-4 col-sm-6">
                        <a class="btn btn-success btn-block" href="{{ route('varasalvs.show',$item->id)}}">{{$item->pealkiri}}</a>
                    </div>
                    <div class="col-md-8 col-sm-6">
                        <p>{{$item->lyhikirjeldus}}</p>
                    </div>   
                </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer text-muted d-flex justify-content-center">
            <span>{{$data->links()}}</span>
        </div>

    @endsection

