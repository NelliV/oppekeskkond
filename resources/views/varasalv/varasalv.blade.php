@extends('layouts.app')

@section('content')

<div class="card-header">
    <div class="float-left"><h2>Varasalv</h2></div>
    <div class="float-right">
        <a class="btn btn-success float-right" href="{{ url('/varasalv/search') }}">
            <i class="fa fa-search"></i>{{ __('  Otsi Varasalvest...') }}
        </a>
    </div>
</div>

<div class="card-body">
    <div class="container-fluid">
        <div class="row top-buffer">
            <div class="col-md-4 col-sm-6">
                <a class="btn btn-success btn-block" href="{{ url('/varasalv/abielu') }}">{{ __('Abielu') }}</a>
            </div>
            <div class="col-md-8 col-sm-6">
            Siia on koondatud kristliku abielu seletus ja nõuanded selle teekonna paremaks läbimiseks
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-md-4 col-sm-6">
                <a class="btn btn-success btn-block" href="{{ url('/varasalv/lastekasvatus') }}">{{ __('Lastekasvatus') }}</a>
            </div>
            <div class="col-md-8 col-sm-6">
            Milline on kristlik lastekasvatus? Mida loeme Piiblist selle kohta?
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-md-4 col-sm-6">
                <a class="btn btn-success btn-block" href="{{ url('/varasalv/elujasurm') }}">{{ __('Elu ja surm') }}</a>
            </div>
            <div class="col-md-8 col-sm-6">
            Inimese surelikkus ja selle maailma kaduvus. Kuidas toime tulla surmaga?
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-md-4 col-sm-6">
                <a class="btn btn-success btn-block" href="{{ url('/varasalv/isiksus') }}">{{ __('Isiksus') }}</a>
            </div>
            <div class="col-md-8 col-sm-6">
            Materjalid, mis aitavad kristlasena kasvada
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-md-4 col-sm-6">
                <a class="btn btn-success btn-block" href="{{ url('/varasalv/suhted') }}">{{ __('Suhted') }}</a>
            </div>
            <div class="col-md-8 col-sm-6">
            Kuidas saame paremini hoida suhteid lähedastega ja meid ümbritsevaga.
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-md-4 col-sm-6">
                <a class="btn btn-success btn-block" href="{{ url('/varasalv/kirik') }}">{{ __('Kirik') }}</a>
            </div>
            <div class="col-md-8 col-sm-6">
            Mina ja kogudus. Mida peaks kristlikkust kirikust teadma?
            </div>
        </div>

    <!--<div class="row top-buffer">
            <form action="{{URL::to('/varasalv/alfabeet')}}" method="POST" role="search">
                <a class="btn btn-success btn-sm" href="{{ url('/varasalv/alfabeet') }}" type="submit"  role="button" id="a">A</a>
                <a class="btn btn-success btn-sm" href="{{ url('/varasalv/alfabeet') }}" type="submit"  role="button" id="b">B</a>
                <a class="btn btn-success btn-sm" href="{{ url('/varasalv/alfabeet') }}" type="submit"  role="button" id="c">C</a>
                <a class="btn btn-success btn-sm" href="{{ url('/varasalv/alfabeet') }}" type="submit"  role="button" id="d">D</a>
                <a class="btn btn-success btn-sm" href="#" role="button">E</a>
                <a class="btn btn-success btn-sm" href="#" role="button">F</a>
                <a class="btn btn-success btn-sm" href="#" role="button">G</a>
                <a class="btn btn-success btn-sm" href="#" role="button">H</a>
                <a class="btn btn-success btn-sm" href="#" role="button">I</a>
                <a class="btn btn-success btn-sm" href="#" role="button">J</a>
                <a class="btn btn-success btn-sm" href="#" role="button">K</a>
                <a class="btn btn-success btn-sm" href="#" role="button">L</a>
                <a class="btn btn-success btn-sm" href="#" role="button">M</a>
                <a class="btn btn-success btn-sm" href="#" role="button">N</a>
                <a class="btn btn-success btn-sm" href="#" role="button">O</a>
                <a class="btn btn-success btn-sm" href="#" role="button">P</a>
                <a class="btn btn-success btn-sm" href="#" role="button">R</a>
                <a class="btn btn-success btn-sm" href="#" role="button">S</a>
                <a class="btn btn-success btn-sm" href="#" role="button">T</a>
                <a class="btn btn-success btn-sm" href="#" role="button">U</a>
                <a class="btn btn-success btn-sm" href="#" role="button">V</a>
                <a class="btn btn-success btn-sm" href="#" role="button">Õ</a>
                <a class="btn btn-success btn-sm" href="#" role="button">Ä</a>
                <a class="btn btn-success btn-sm" href="#" role="button">Ö</a>
                <a class="btn btn-success btn-sm" href="#" role="button">Ü</a>
            </form>
        </div>-->
    </div>
</div>

@endsection