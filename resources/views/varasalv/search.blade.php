@extends('layouts.app')

@section('content')

<div class="card-header">
    <div class="float-left"><h2>Otsi varasalvest...</h2></div>
    <div class="float-right">
        <a class="btn btn-success" href="{{ url('/varasalv') }}">
            <i class="fa fa-backspace"></i>{{ __('  Varasalv') }}
        </a>
    </div>
</div>

<div class="card-body">
    <div class="container-fluid">
        <!--Searchbar-->
        <form action="{{URL::to('/search')}}" method="POST" role="search">
            {{ csrf_field() }}
            <div class="input-group col-md-12">
                <input type="text" class="form-control" name="q" 
                    placeholder="Otsi..." ><span class="input-group-btn">
                    <button type="submit" class="btn btn-success" >
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>

        @if(isset($details))
            <p> Sinu otsingu "<b> {{ $query }} </b>" tulemused on : </p>
            <table class="table table-stripped">
                <tbody>
                    @foreach($details as $varasalv)
                    <tr>
                        <td> <a class="btn btn-success btn-block" href="{{ route('varasalvs.show',$varasalv->id)}}">{{$varasalv->pealkiri}}</a> </td>
                        <td> {{ $varasalv->lyhikirjeldus}} </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @elseif(isset($message))
            <p>{{ $message}} </p>
        @endif

    </div>
</div>

@endsection