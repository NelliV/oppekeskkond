<!-- index.blade.php -->

@extends('layouts.app')

@section('content')

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <a class="btn btn-outline-success btn-block" href="{{ url('/varasalv/create') }}">
                {{ __('  Loo uus') }}</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Kategooria</td>
          <td>Pealkiri</td>
          <td>Lühikirjeldus</td>
          <td colspan="3">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($varasalvs as $varasalv)
        <tr>
            <td>{{$varasalv->id}}</td>
            <td>{{$varasalv->kategooria}}</td>
            <td>{{$varasalv->pealkiri}}</td>
            <td>{{$varasalv->lyhikirjeldus}}</td>
            <td><a href="{{ route('varasalvs.edit',$varasalv->id)}}" class="btn btn-primary">Muuda</a></td>
            <td><a href="{{ route('varasalvs.show',$varasalv->id)}}" class="btn btn-success">Vaata</a></td>
            <td>
                <form action="{{ route('varasalvs.destroy', $varasalv->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Kustuta</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
@endsection