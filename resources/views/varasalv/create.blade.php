<!-- create.blade.php -->

@extends('layouts.app')

@section('stylesheets')

  <style>
    .uper {
      margin-top: 40px;
    }
  </style>
@endsection

@section('content')

<div class="card uper">
  <div class="card-header">
    Lisa õpetus
    <div class="float-right">
        <a class="btn btn-success" href="{{ (url()->previous()) }}">
            <i class="fa fa-backspace"></i>{{ __('  Tagasi') }}
        </a>
    </div>
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('varasalvs.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Kategooria :</label>
              <input type="text" class="form-control" name="kategooria"/>
          </div>
          <div class="form-group">
              <label for="price">Pealkiri :</label>
              <input type="text" class="form-control" name="pealkiri"/>
          </div>
          <div class="form-group">
              <label for="quantity">Lühikirjeldus :</label>
              <input type="text" class="form-control" name="lyhikirjeldus"/>
          </div>
          <div class="form-group">
              <label for="quantity">Tekst :</label>
              <input id="mytextarea" type="textarea" class="form-control" name="tekst"/>
          </div>
          <button type="submit" class="btn btn-success">Loo õpetus</button>
      </form>
  </div>
</div>
@endsection