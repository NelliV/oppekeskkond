@extends('layouts.app')

@section('content')

<div class="card-header">
    <div class="float-left"><h2>Tähestikuline õpetus...</h2></div>
    <div class="float-right">
        <a class="btn btn-success" href="{{ url('/varasalv') }}">
            <i class="fa fa-backspace"></i>{{ __('  Varasalv') }}
        </a>
    </div>
</div>

<div class="card-body">
    <div class="container-fluid">
    @if(isset($details))
            <p> Sinu tähe "<b> {{ $query }} </b>" kohta on järgmised teemad : </p>
            <table class="table table-stripped">
                <tbody>
                    @foreach($details as $varasalv)
                    <tr>
                        <td> <a class="btn btn-success btn-block" href="{{ route('varasalvs.show',$varasalv->id)}}">{{$varasalv->pealkiri}}</a> </td>
                        <td> {{ $varasalv->lyhikirjeldus}} </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @elseif(isset($message))
            <p>{{ $message}} </p>
        @endif
    </div>
</div>

@endsection


