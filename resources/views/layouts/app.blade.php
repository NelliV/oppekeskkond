<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Õppekeskus') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src='https://cloud.tinymce.com/5/tinymce.min.js?apiKey=e89qjim0coq67iz38yzqltrwtd3g7ey54ppg34j3705w3tc7'></script>
        <script>
        tinymce.init({
            selector: '#mytextarea'
        });
        </script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <style>
            .js-cookie-consent{
                position: absolute;
                top: 0px;
                padding: 10px;
                text-align: center;
                width: 100%;
                z-index: 9999;
                background-color: #fffbdb;
                border-color: #fffacc;
                border: solid 1px;
            }
            html, body {
                background-color: #fffdf1;
                color: #636b6f;
                font-family: 'Merriweather', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                }

            .top-buffer { margin-top:20px; }

            .page-item.active .page-link {
                background-color: #38c172;
                border-color: #38c172;
                }
            
            .page-link {
                color: #38c172;
                }
        </style>
    </head>
    <body>
        @include('cookieConsent::index')
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('http://rannu.eelk.ee/') }}">
                        {{ 'Koduleht' }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        @if(Auth::check())
                        <ul class="navbar-nav mr-auto">
                            <li>
                                <a class="nav-link" href="{{ url('/sissejuhatus/ajalugu') }}">{{ __('Sissejuhatus') }}</a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ url('/varasalv') }}">{{ __('Varasalv') }}</a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ url('/kkk') }}">{{ __('KKK') }}</a>
                            </li>
                            @if(Auth::user()->admin == '1')
                            <li>
                                <a class="nav-link" href="{{ url('/admin') }}">{{ __('Admin') }}</a>
                            </li>
                            @endif
                        </ul>
                        @endif

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                            <li><a class="nav-link" href="{{ route('login') }}">Logi sisse</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">Registreeri</a></li>
                            @else
                            <li class="nav-item">  
                                <a class="nav-link" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logi välja') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>

            <main class="py-4">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="card">
                                <!--@if(Auth::check()) -->
                                @if (\Request::is('sissejuhatus/*') or (\Request::is('/')))
                                <div class="card-header">
                                    <ul class="nav nav-tabs card-header-tabs">
                                        <li><a class="{{ (\Request::is('sissejuhatus/ajalugu') or (\Request::is('/'))) ? 'active btn btn-outline-success nav-link' : 'btn btn-outline-success nav-link' }}" href="{{ url('/sissejuhatus/ajalugu') }}">{{ __('Ajalugu') }}</a></li>
                                        <li><a class="{{ Request::is('sissejuhatus/ristimine') ? 'active btn btn-outline-success nav-link' : 'btn btn-outline-success nav-link' }}" href="{{ url('/sissejuhatus/ristimine') }}">{{ __('Ristimine') }}</a></li>
                                        <li><a class="{{ Request::is('sissejuhatus/armulaud') ? 'active btn btn-outline-success nav-link' : 'btn btn-outline-success nav-link' }}" href="{{ url('/sissejuhatus/armulaud') }}">{{ __('Armulaud') }}</a></li>
                                        <li><a class="{{ Request::is('sissejuhatus/palve') ? 'active btn btn-outline-success nav-link' : 'btn btn-outline-success nav-link' }}" href="{{ url('/sissejuhatus/palve') }}">{{ __('Palve') }}</a></li>
                                        <li><a class="{{ Request::is('sissejuhatus/piibel') ? 'active btn btn-outline-success nav-link' : 'btn btn-outline-success nav-link' }}" href="{{ url('/sissejuhatus/piibel') }}">{{ __('Piibel') }}</a></li>
                                        <li><a class="{{ Request::is('sissejuhatus/10kasku') ? 'active btn btn-outline-success nav-link' : 'btn btn-outline-success nav-link' }}" href="{{ url('/sissejuhatus/10kasku') }}">{{ __('10 Käsku') }}</a></li>
                                        <li><a class="{{ Request::is('sissejuhatus/usutunnistus') ? 'active btn btn-outline-success nav-link' : 'btn btn-outline-success nav-link' }}" href="{{ url('/sissejuhatus/usutunnistus') }}">{{ __('Usutunnistus') }}</a></li>
                                    </ul>
                                </div>
                                @endif
                                <!--@endif-->
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </body>
</html>
