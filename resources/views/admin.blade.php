@extends('layouts.app')

@section('content')

<!--@if(Auth::check()) -->
<div class="card-header card bg-success text-white">Admin</div>
    <div class="card-body shadow">
        <a href="{{ url('/users') }}" class="btn btn-outline-success btn-lg btn-block">Halda kasutajaid</a>
        <a href="{{ url('/varasalvs') }}" class="btn btn-outline-success btn-lg btn-block">Halda õppematerjali</a>
    </div>
<!--@endif-->

@endsection
