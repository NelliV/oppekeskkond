@extends('layouts.app')

@section('content')
<!-- KIRIKU AJALUGU -->
    @if(Auth::check())
    <div class="card-body shadow ">
        <div class="tab-pane fade show active">
            <div class="row" style="padding: 1.5em;">
                <div class="col-md-6 embed-responsive embed-responsive-16by9" >
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/xFIXMM1KWyc"  allowfullscreen></iframe>
                </div>
                <div class="col-md-6 text-center" >
                    <a style="margin: 2em; background-color:rgba(0,0,0,.09);"  data-toggle="modal" data-target="#test" class="btn btn-outline-success" href="#test">Testi ennast!</a>
                    <!-- Siin on Kiriku Ajaloo test -->
                    <div class = "modal fade" id = "test" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
                        <div class = "modal-dialog">
                            <div class = "modal-content">
                                <div class = "modal-header">
                                    <h4 class = "modal-title" id = "myModalLabel">
                                        Kiriku ajaloo test
                                    </h4>
                                    <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                        &times;
                                    </button>
                                </div>
                                <div class = "modal-body">            
                                    <label>1. Esimese paavst oli Paul</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right1" role="button">
                                            Õige
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong1" role="button">
                                            Vale
                                        </a>
                                    </p>
                                    <div id="accordion1">
                                        <div class="collapse" id="right1" data-parent="#accordion1">
                                            <div class="card card-body">
                                                Ei see ei olnud niimoodi. Proovi uuesti.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong1" data-parent="#accordion1">
                                            <div class="card card-body">
                                                Vastasid õigesti. Esimese paavst oli hoopis Peeter
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>2. Kristliku kiriku asutas Peeter</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right2" role="button">
                                            Õige
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong2" role="button">
                                            Vale
                                        </a>
                                    </p>
                                    <div id="accordion2">
                                        <div class="collapse" id="right2" data-parent="#accordion2">
                                            <div class="card card-body">
                                                Ei, see ei olnud Peeter. Proovi uuesti.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong2" data-parent="#accordion2">
                                            <div class="card card-body">
                                                Tõesti. Väide on väär. Kristlikule kirikule pani aluse Jeesus Kristus.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>3. Apostlitest suri viimasena Johannes</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right3" role="button">
                                            Õige
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong3" role="button">
                                            Vale
                                        </a>
                                    </p>
                                    <div id="accordion3">
                                        <div class="collapse" id="right3" data-parent="#accordion3">
                                            <div class="card card-body">
                                                See on tõesti nii
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong3" data-parent="#accordion3">
                                            <div class="card card-body">
                                                See on väär vastus. Palun proovi uuesti.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>4. Kala oli varase kristluse sümbol</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right4" role="button">
                                            Õige
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong4" role="button">
                                            Vale
                                        </a>
                                    </p>
                                    <div id="accordion4">
                                        <div class="collapse" id="right4" data-parent="#accordion4">
                                            <div class="card card-body">
                                                See väide on õige. on Kristuse sümbol, sest kreeka keeles annavad sõnade "Jeesus, Kristus, Jumala Poeg, Päästja" 
                                                esitähed kokku IHTYS, mis tähendab kala. Kolm kala tähistavad Püha Komainsust: Isa, Poega ja Püha Vaimu.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong4" data-parent="#accordion4">
                                            <div class="card card-body">
                                                See on väär vastus. Palun proovi uuesti.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>5. Nero süüdistas kristlasi Roomat tabanud tulekahjus aastal 54</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right5" role="button">
                                            Õige
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong5" role="button">
                                            Vale
                                        </a>
                                    </p>
                                    <div id="accordion5">
                                        <div class="collapse" id="right5" data-parent="#accordion5">
                                            <div class="card card-body">
                                                See on tõesti nii
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong5" data-parent="#accordion5">
                                            <div class="card card-body">
                                                Vale vastus. Proovi uuesti.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-footer">
                                    <button type = "button" class = "btn btn-default" data-dismiss = "modal">
                                        Tagasi
                                    </button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </div>
            </div>
            <div class="row" style="padding: 1.5em;">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Aeg</th>
                            <th scope="col">Sündmused</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>33</td>
                            <td>Nelipühi sündmus. Ristikoguduse ajalooline algus.</td>
                        </tr>
                        <tr>
                            <td>c.33/36</td>
                            <td>Saulusest saab Paulus</td>
                        </tr>
                        <tr>
                            <td>35</td>
                            <td>Nimetust <i>kristlane</i>  kasutatakse esmakordselt Antiookias</td>
                        </tr>
                        <tr>
                            <td>45-49</td>
                            <td>Pauluse esimene misjonireis: Antiookia - Küpros - Väike Aasia/ Galaatia.</td>
                        </tr>
                        <tr>
                            <td>ca 50</td>
                            <td>Apostlite konvent Jeruusalemas.</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Algristikogudus – suuremad keskused: Jeruusalem, Antiookia , Aleksandria, Kartaago, Kontsantinoopol, Rooma…</td>
                        </tr>
                        <tr>
                            <td>64-311</td>
                            <td>64 suur tagakiusamine keiser Nero ajal. Tagakiusamine jätkub vaheaegadega ja erineva raskusega. Viimane suurem tagakiusamine 299-311. Julmim tagakiusamine 303.a. keiser Diocletianuse ajal. Veretunnmistajaid-märtreid  hinntakse kogu tagakiusamise perioodil l kokku 3000 - 3500</td>
                        </tr>
                        <tr>
                            <td>64</td>
                            <td>Peetrus ja Paulus hukatakse Roomas keiser Neero tagakiusamise ajal (64-67)  </td>
                        </tr>
                        <tr>
                            <td>75</td>
                            <td>Judea, Galilea ja  Samaaria nimetatakse roomlaste poolt ümber <i>Palestiina.</i></td>
                        </tr>
                        <tr>
                            <td>310</td>
                            <td>Armeenlastest saab esimene kristlik rahvus.</td>
                        </tr>
                        <tr>
                            <td>313</td>
                            <td>Constantin Suur ja Licinius annavad välja Milano dekreedi/edikti, milles ametlikult tunnustatkse usuvabadust Rooma impeeriumis; eraldi märgitakse ära  religioosset tolerantsi kristluse suhtes, kristlikute kirikute omandi restauaratsiooni ja legaalset tunnustamist.</td>
                        </tr>
                        <tr>
                            <td>ca 320</td>
                            <td>Munkluse algus. Eremiit St. Antonius Egiptuses kirjutab esimesed reeglid.</td>
                        </tr>
                        <tr>
                            <td>381</td>
                            <td>Konstantinoopoli kirikukogu otsustab, et Konstantinoopoli patriarh on autoriteedilt kogu kirikus Rooma piiskopi järel teine.</td>
                        </tr>
                        <tr>
                            <td>380-395</td>
                            <td>Theodosius I (*346 - +395)  ajal saab  kristlus  Rooma riigi riiklikuks usundiks. Võitlus paganlike kultustegaja hereetikutega (ariaanlus,  Mani õpetus). 391 paganlikud templid suletakse ja kultused keelatakse. </td>
                        </tr>
                        <tr>
                            <td>IV-VI s.</td>
                            <td>Kloostrite ja munkluse algus ja esmane areng Euroopas.</td>
                        </tr>
                        <tr>
                            <td>*354 - +430</td>
                            <td>Augustiinus Hippost üks suurematest varakristliku aja mõtlejatest.</td>
                        </tr>
                        <tr>
                            <td>ca ... 988 ...</td>
                            <td>Venemaa ristiusustamine, oletatavad varasemad  laiemad kokkupuuted ristiusuga, 1030 on andmed eestlaste ristimisest</td>
                        </tr>
                        <tr>
                            <td>1054</td>
                            <td>Kiriku jagunemine Ida kirikuks keskusega Bütsants  ja Lääne kirikuks  keskusega Rooma</td>
                        </tr>
                        <tr>
                            <td>1062-64</td>
                            <td>Breemeni peapiiskopi Adalbert`i poolt määratuna on  eestlaste ja kurelaste piiskopiks munk  Hiltuin. Kartes oma hingeõnnistuse pärast „väärjumalate ägedate teenijate“ seas ta loobub oma ametist.</td>
                        </tr>
                        <tr>
                            <td>1096-1291</td>
                            <td>Ristisõjad, kokku 9,  Püha Maa ja Jeruusalema vabastamiseks</td>
                        </tr>
                        <tr>
                            <td>1165</td>
                            <td>Lundi  piiskop Eskil pühitseb benediktiini munga Fulco  eestlaste piiskopiks. 1171.a. palub paavst Aleksander III Norra peapiiskopi saata Fulcole abiliseks   munk Nikolaus Stavangeri kloostrist Norramaalt, kes räägib eesti keelt. Ühes oma samaaegses kirjas  manitseb paavst  Taanimaal olevaid usklikke abistama Fulcot, kes kannatab suurt vaesust ja puudust. 1164-1178 käib Fulco kolmel korral Eestimaal misjoniretketel, kuid tuleb neilt tagasi  tagasi väsinuna ilma eriliste tulemusteta.</td>
                        </tr>
                        <tr>
                            <td>1184</td>
                            <td>Inkvisitsiooni algus.</td>
                        </tr>
                        <tr>
                            <td>1202</td>
                            <td>Paavst kuulutab ristisõja Maarjamaaks nimetatud Vana-Liivimaa ristusku pööramiseks. Piiskop Albert asutab Mõõgavendade ordu.<br />
                            Ajaloolane Ivar Leimus seletab Balti ristisõja puhkemist Euroopa paisumise ja eluruumi kitsaks jäämisega</td>
                        </tr>
                        <tr>
                            <td>1208-1224</td>
                            <td>Saksa –Rootsi- Taani  vallutussõda Eesti alade allutamiseks. 1224 on mandri-Eesti vallutatud, 1227 langeb viimane vabaduse kants  Saaremaa.</td>
                        </tr>
                        <tr>
                            <td>1224-1561</td>
                            <td>Ordu ja piiskopide aeg. 1599 müüakse Saare-Lääne piiskopkond (salalepinguga) Taanile.</td>
                        </tr>
                        <tr>
                            <td>1415</td>
                            <td>Tśehhi usureformaator Jan Hus (*ca 1370), reformatsiooni eelkäija, põletatakse Konstanzi kirikukogu otsusega ketserina tuleriidal.</td>
                        </tr>
                        <tr>
                            <td>1453</td>
                            <td>Pärast Konstantinoopoli langemist aastal 1453 hakkasid venelased rõhutama Moskva tähtsust “kolmanda Roomana”. Selles idees ühines poliitiline ja religioosne ideoloogia. <br />
                            Moskva jätkas Bütsantsi pärandit, milles kirik on ühiskonna lahutamatu osa.</td>
                        </tr>
                        <tr>
                            <td>1455</td>
                            <td>Seni käsitsi kirjutatud Piibli kõrvale tuleb esimene Gutenbergi poolt trükitud Piibel . See avab võimaluse Piibli ja muu trükisõna laiemaks levikuks rahva hulka.</td>
                        </tr>
                        <tr>
                            <td>1484</td>
                            <td>Paavst Innocentius VIII annab inkvisitsioonile loa Saksamaal alustada nõidadedele ja hereetikutele jaht, etteotsa määratakse Heinrich Kramer ja Jacob Sprenger. 1487 annavad nad välja "Malleus Maleficarum" ("Nõiahaamer"), mis sai Euroopa nõiaprotsesside tähtsaimaks alusteoseks. Raamatus väideti, et nõiad tõepoolest eksisteerivad, ning seejärel õpetati, kuidas neid avastada, üle kuulata ja seejärel karistada.<br />
                            Nőidade surmamőistmisel toetuti suuresti Saksa  Rooma  kriminaalseadusele "Constitutio Criminalis Carolina" 1532, kus seisis, et nőidumise abil kahju tekitamise korral tuleb süüdlane pőletada tuleriidal.<br />
                            Nõiajahi kõrgaeg oli aastatel 1580–1630. Viimane teadaolev hukkamine Euroopas toimus Šveitsis 1782; Eestis 1699. aastal.<br />
                            Nõiajahi ohvrite arvu  hinnatakse ca 40 000 – 60 000; suur osa on siin protestantlikel maadel.</td>
                        </tr>
                        <tr>
                            <td>31.oktoober 1517</td>
                            <td>Wittenbergi lossi kabeli uksele 95 teesi. Luterliku kiriku algus ja sünnipäev.<br />
                            Kiriku jagunemine reformatsioonis: Anglikaani kirik (1529), Luterlik kirik, Kalvinistlik kirik</td>
                        </tr>
                        <tr>
                            <td>1523</td>
                            <td>Dr. Martin Lutheri kiri „Äravalitud armsatele Jumala sõpradele Riias,Tallinnas,Tartus Liivimaal“ Esimesed reformatsiooni vaimust kantud jutlused Niguliste ja Oleviste kirikus.</td>
                        </tr>
                        <tr>
                            <td>1524</td>
                            <td>Reformatsiooni algus Tallinnas ja Tartus</td>
                        </tr>
                        <tr>
                            <td>1525</td>
                            <td>Teated esimesest eesti keelsest vaimuliku sisuga trükisest.</td>
                        </tr>
                        <tr>
                        
                            <td>1530</td>
                            <td>1530 Augsburg'i konfessioon/usutunnistus.</td>
                        </tr>
                        <tr>
                            <td>1535</td>
                            <td>Tallinnas ilmub eesti keeles kirikuõpetaja Simon Wanradt`i ja köster Johann Koell`i katekismus</td>
                        </tr>
                        <tr>
                            <td>1540</td>
                            <td>Ignatius Loyola rajab reformatsiooni vastu võitlemiseks jesuiitide ordu</td>
                        </tr>
                        <tr>
                            <td>1555</td>
                            <td>Augsburgi rahu katoliku Püha Rooma Impeeriumi ja protestantliku Schmalkaldeni Liiga vahel. Vürstidel jääb vabadus usku valida, rahva jaoks aga jääb  kehtima põhimõte "Kelle valitsus, selle usk".</td>
                        </tr>
                        <tr>
                            <td>1558-1625</td>
                            <td>Sõdade periood Liivimaal, milles osalevad  Venemaa, Poola, Rootsi ja Taani. 1582-1629? Liivimaal  Poola aeg- vastureformatsioon.</td>
                        </tr>
                        <tr>
                            <td>1572</td>
                            <td>24. augustil, Bartholomeuse ööl, tapetakse Pariisis katoliiklaste poolt ca 3000 hugenotti/protestanti-kalvinisti ; jätkuvalt kuni oktoobrini tapetakse ca 70 000 hugenotti kogu Prantsusmaal.</td>
                        </tr>
                        <tr>
                            <td>1616-1648</td>
                            <td>30 aastane sõda Saksamaal katoliku Püha Liiga ja protestantliku Evangeelse Uniooni  vahel. Tulemuseks 1555.a. rahust lähtuvad kokkulepped katoliiklaste - protestantide vahel ja sõjast laastatud Saksamaa</td>
                        </tr>
                        <tr>
                            <td>1625-1710</td>
                            <td>Rootsi aeg Eestis</td>
                        </tr>
                        <tr>
                            <td>1710-1917</td>
                            <td>Eestis Tsaari Venemaa valitsemise aeg</td>
                        </tr>
                        <tr>
                            <td>1715</td>
                            <td>Ilmub põhjaeesti murdes  Uus Testament</td>
                        </tr>
                        <tr>
                            <td>1727</td>
                            <td>Vennastekoguduse rajamine Saksamaal krahv Nicolaus Zizendorf`i poolt</td>
                        </tr>
                        <tr>
                            <td>1739</td>
                            <td>Ilmub esimene täielik eesti keelne Piibel</td>
                        </tr>
                        <tr>
                            <td>XVII-XIX saj.</td>
                            <td>Protestantliku kiriku jagunemine – babtistid (XVII s.), metodistid (XVIII s.),  adventistid (XIX s.), ….</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Lisaks sellele on tekkinud palju kirikuid, kes oma idee on sidunud Piibli ja kristlusega, kuid oma õpetustes ja tõekspidamistes ei mahu kristliku kiriku raamidesse - jehoovatunnistajad, uusapostlid, mormoonid, …2006.a. aastal on loetletud … erinevat kristlikku kirikut ja kogudust.</td>
                        </tr>
                        <tr>
                            <td>1854</td>
                            <td>Neitsist sünni dogma  katoliku kirikus</td>
                        </tr>
                        <tr>
                            <td>1870</td>
                            <td>Paavsti ilmeksimatuse dogma katoliku kirikus</td>
                        </tr>
                        <tr>
                            <td>31.05– 01.06. 1917</td>
                            <td>I Kirikukongress Tartus. Esitatakse kava kiriku uueks korralduseks ja valitakse komisjon selle kava läbitöötamiseks  ja  kiriku uue põhikirja väljatöötamiseks</td>
                        </tr>
                        <tr>
                            <td>10.IX-12.IX  1919</td>
                            <td>II Kirikukongress Tallinnas. EELK põhikirja vastuvõtmine.Eesti Evangeeliumi Luteri Usu Kirik kui vaba rahvakirik. Piiskop Jakob Kukk valitakse piiskopina ametisse .</td>
                        </tr>
                        <tr>
                            <td>1940-41</td>
                            <td>Nõukogude aeg Eestis</td>
                        </tr>
                        <tr>
                            <td>1943</td>
                            <td>Rekonstrueeritud E.E.L.K. Kirikukogu antud volituse sõnastusega.</td>
                        </tr>
                        <tr>
                            <td>1944-1991</td>
                            <td>Nõukogude aeg Eestis</td>
                        </tr>
                        <tr>
                            <td>1947</td>
                            <td>E.E.L.K. organiseerumine pagulaskirikuna.</td>
                        </tr>
                        <tr>
                            <td>1948</td>
                            <td>E.E.L.K. Komitee avaldab EELK Põhimäärused ühes </td>
                        </tr>
                        <tr>
                            <td>1957</td>
                            <td>Euroopas, Põhja- ja Lõuna-Ameerikas, Austraalias eesti kogudusi 62    ca 60 tuh. liikmega</td>
                        </tr>
                        <tr>
                            <td>24. IX 1958</td>
                            <td>Peapiiskop Johan Kõpp taastab kõik E.E.L.K. seaduslikud organid välismaal.</td>
                        </tr>
                        <tr>
                            <td>1991 - ...</td>
                            <td>EELK vabas Eestis</td>
                        </tr>
                    </tbody>
                </table>
                <a href="http://www.eelk.ee/voru.katariina/">Allikas</a>
            </div>
        </div>
    </div>
    @endif
@endsection