@extends('layouts.app')

@section('content')
<!-- ARMULAUD -->
    @if(Auth::check())
    <div class="card-body shadow ">
        <div class="tab-pane  show fade">
            <div class="row" style="padding: 1.5em;">
                <div class="col-md-6 embed-responsive embed-responsive-16by9" >
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/RPBK6yvatSU"  allowfullscreen></iframe>
                </div>
                <div class="col-md-6 text-center" >
                    <a style="margin: 2em; background-color:rgba(0,0,0,.09);"  data-toggle="modal" data-target="#test" class="btn btn-outline-success" href="test">Testi ennast!</a>
                    <!-- Siin on Abielu test -->
                    <div class = "modal fade" id = "test" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
                        <div class = "modal-dialog">
                            <div class = "modal-content">
                                <div class = "modal-header">
                                    <h4 class = "modal-title" id = "myModalLabel">
                                        Armulaua test
                                    </h4>
                                    <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                        &times;
                                    </button>
                                </div>
                                <div class = "modal-body">            
                                    <label>1. Kas usud, et oled patune?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right1" role="button">
                                            Jah, usun
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong1" role="button">
                                            Ei, ei usu
                                        </a>
                                    </p>
                                    <div id="accordion1">
                                        <div class="collapse" id="right1" data-parent="#accordion1">
                                            <div class="card card-body">
                                                Usun, et olen patune.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong1" data-parent="#accordion1">
                                            <div class="card card-body">
                                                Vale, proovi uuesti
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>2. Kust sa seda tead?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right2" role="button">
                                            Kümnest käsust, mille vastu olen eksinud.
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong2" role="button">
                                            Ei tea, ma pole patune
                                        </a>
                                    </p>
                                    <div id="accordion2">
                                        <div class="collapse" id="right2" data-parent="#accordion2">
                                            <div class="card card-body">
                                                Õige
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong2" data-parent="#accordion2">
                                            <div class="card card-body">
                                                Vale vastus. "Sest kõik on pattu teinud ja ilma jäänud Jumala kirkusest" (Rm 3:23)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>3. Kas su patud teevad sulle muret?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right3" role="button">
                                            Teevad
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong3" role="button">
                                            Ei tee
                                        </a>
                                    </p>
                                    <div id="accordion3">
                                        <div class="collapse" id="right3" data-parent="#accordion3">
                                            <div class="card card-body">
                                                Olen mures sellepärast, et olen eksinud Jumala vastu.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong3" data-parent="#accordion3">
                                            <div class="card card-body">
                                                Armulauale minnes peaksid patud ikka muret tegema. Proovi uuesti.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>4. Missuguse karistuse Jumalalt oled sa oma pattudega ära teeninud?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right4" role="button">
                                            Väärin karistust
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong4" role="button">
                                            Ma ei ole karistust väärt
                                        </a>
                                    </p>
                                    <div id="accordion4">
                                        <div class="collapse" id="right4" data-parent="#accordion4">
                                            <div class="card card-body">
                                                Olen ära teeninud Tema viha ja halastamatuse, surma ja igavese hukatuse.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong4" data-parent="#accordion4">
                                            <div class="card card-body">
                                                Armulauale tulles peaks seda endale lahti mõtlema. Proovi uuesti.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>5. Soovid sa siiski pääseda?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right5" role="button">
                                            Jah
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong5" role="button">
                                            Ei
                                        </a>
                                    </p>
                                    <div id="accordion5">
                                        <div class="collapse" id="right5" data-parent="#accordion5">
                                            <div class="card card-body">
                                                Õige vastus!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong5" data-parent="#accordion5">
                                            <div class="card card-body">
                                                Proovi uuesti!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "modal-body">            
                                    <label>6. Millel põhineb see sinu soov?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right6" role="button">
                                            Issanda Jeesuse Kristuse tõotusel.
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong6" role="button">
                                            Sisetundel
                                        </a>
                                    </p>
                                    <div id="accordion6">
                                        <div class="collapse" id="right6" data-parent="#accordion6">
                                            <div class="card card-body">
                                                Õige vastus!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong6" data-parent="#accordion6">
                                            <div class="card card-body">
                                                Proovi uuesti!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>7. Kes on Kristus?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right7" role="button">
                                            Ta on Jumala Poeg, tõeline Jumal ja tõeline inimene.
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong7" role="button">
                                            Ta on lihtsalt kunagi elanud inimene. Ei midagi enamat.
                                        </a>
                                    </p>
                                    <div id="accordion7">
                                        <div class="collapse" id="right7" data-parent="#accordion7">
                                            <div class="card card-body">
                                                Õige vastus!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong7" data-parent="#accordion7">
                                            <div class="card card-body">
                                                Proovi uuesti!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>8. Kes on Kristus?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right8" role="button">
                                            Ta on Jumala Poeg, tõeline Jumal ja tõeline inimene.
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong8" role="button">
                                            Ta on lihtsalt kunagi elanud inimene. Ei midagi enamat.
                                        </a>
                                    </p>
                                    <div id="accordion8">
                                        <div class="collapse" id="right8" data-parent="#accordion8">
                                            <div class="card card-body">
                                                Õige vastus!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong8" data-parent="#accordion8">
                                            <div class="card card-body">
                                                Proovi uuesti!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>9. Mida on Kristus sinu heaks teinud?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right9" role="button">
                                            Ta on valanud minu eest ristil oma verd pattude andeksandmise nimel.
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong9" role="button">
                                            Ta on surnud minu eest.
                                        </a>
                                    </p>
                                    <div id="accordion9">
                                        <div class="collapse" id="right9" data-parent="#accordion9">
                                            <div class="card card-body">
                                                Õige vastus! Tubli!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong9" data-parent="#accordion9">
                                            <div class="card card-body">
                                                Ka see on õige vastus!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>10. Kuidas sa seda tead?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right10" role="button">
                                            Evangeeliumist.
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong10" role="button">
                                            Püha altarisakramendi seadmissõnadest.
                                        </a>
                                    </p>
                                    <div id="accordion10">
                                        <div class="collapse" id="right10" data-parent="#accordion10">
                                            <div class="card card-body">
                                                Õige vastus! Tubli!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong10" data-parent="#accordion10">
                                            <div class="card card-body">
                                                Ka see on õige vastus!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>11. Kas sa tunned armulaua seadmissõnu?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right11" role="button">
                                            Jah
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong11" role="button">
                                            Ei
                                        </a>
                                    </p>
                                    <div id="accordion11">
                                        <div class="collapse" id="right11" data-parent="#accordion11">
                                            <div class="card card-body">
                                                Tubli!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong11" data-parent="#accordion11">
                                            <div class="card card-body">
                                                Armulaua seadmissõnad on järgnevad: "Meie Issand Jeesus Kristus, sel ööl, 
                                                mil Tema ära anti, võttis leiva, tänas, murdis, andis oma jüngritele ja ütles: 
                                                "Võtke, sööge, see on minu ihu, mis teie eest antakse! Tehke seda minu mälestuseks!" 
                                                Selsamal kombel võttis Ta karika pärast õhtusöömaaega, tänas, andis neile ja ütles: 
                                                "Võtke, jooge sellest kõik, see karikas on uus leping minu veres, mis teie eest valatakse 
                                                pattude andeksandmiseks! Nii sagedasti, kui te sellest joote, tehke seda minu mälestuseks!"
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>12. Kas sa usud, et selles sakramendis antakse sulle Kristuse tõeline ihu ja tõeline veri?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right12" role="button">
                                            Jah, usun.
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong12" role="button">
                                            Ei, ei usu.
                                        </a>
                                    </p>
                                    <div id="accordion12">
                                        <div class="collapse" id="right12" data-parent="#accordion12">
                                            <div class="card card-body">
                                                Tubli!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong12" data-parent="#accordion12">
                                            <div class="card card-body">
                                                Proovi uuesti!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>13. Mis sunnib sind niiviisi uskuma?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right13" role="button">
                                            Kristuse sõna: Võtke ja sööge, see on minu ihu; võtke ja jooge, see on minu veri.
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong13" role="button">
                                            Mitte midagi ei sunni.
                                        </a>
                                    </p>
                                    <div id="accordion13">
                                        <div class="collapse" id="right13" data-parent="#accordion13">
                                            <div class="card card-body">
                                                Tubli! Vastasid õigesti.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong13" data-parent="#accordion13">
                                            <div class="card card-body">
                                                Proovi uuesti!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>14. Mida me teeme armulaual osaledes?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right14" role="button">
                                            Me kuulutame Kristuse surma.
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong14" role="button">
                                            Meenutame Tema sõnu: "Nii sagedasti, kui te sellest joote, tehke seda minu mälestuseks!"
                                        </a>
                                    </p>
                                    <div id="accordion14">
                                        <div class="collapse" id="right14" data-parent="#accordion14">
                                            <div class="card card-body">
                                                Tubli! Vastasid õigesti.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong14" data-parent="#accordion14">
                                            <div class="card card-body">
                                                Tubli! Ka see on õige vastus!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>15. Jah me peame meenutama ja kuulutama Jeesuse surma?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right15" role="button">
                                            Jah
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong15" role="button">
                                            Ei
                                        </a>
                                    </p>
                                    <div id="accordion15">
                                        <div class="collapse" id="right15" data-parent="#accordion15">
                                            <div class="card card-body">
                                                Tubli! Vastasid õigesti. Selleks, et hakkaksime tõesti taipama, et mitte keegi looduist 
                                                ei ole saanud meie patte lepitada, vaid ainuüksi meie Issand Jeesus Kristus, ja et õpiksime 
                                                hindama oma eksimusi ränkadeks, aga Tema armu veelgi suuremaks.
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong15" data-parent="#accordion15">
                                            <div class="card card-body">
                                                Vale! Proovi uuesti!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>16. Kas miski sundis Kristust meie patte lepitama?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right16" role="button">
                                            Ei
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong16" role="button">
                                            Jah
                                        </a>
                                    </p>
                                    <div id="accordion16">
                                        <div class="collapse" id="right16" data-parent="#accordion16">
                                            <div class="card card-body">
                                                Vale! Proovi uuesti!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong16" data-parent="#accordion16">
                                            <div class="card card-body">
                                                Õige! See oli Tema suur armastus oma Isa vastu, samuti Tema armastus minu ja kõigi teiste patuste vastu, 
                                                nii nagu on kirjutatud: "Kristus on armastanud mind ja ohverdanud enda minu eest" (Gl 2:20).
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>17. Mis õhutab sind tulema võimalikult sageli armulauale?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right17" role="button">
                                            Issanda Jeesuse Kristuse käsk ja tõotus.
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong17" role="button">
                                            Omad mured.
                                        </a>
                                    </p>
                                    <div id="accordion17">
                                        <div class="collapse" id="right17" data-parent="#accordion17">
                                            <div class="card card-body">
                                                Õige!
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong17" data-parent="#accordion17">
                                            <div class="card card-body">
                                                Õige!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class = "modal-body">            
                                    <label>18. Mida peab tegema niisugune inimene, kes ei tunne muret oma pattude pärast ega janune sakramendi järele?</label>
                                    <p>
                                        <a class="btn btn-outline-success" data-toggle="collapse" href="#right18" role="button">
                                            Midagi peab ette võtma!
                                        </a>
                                        <a class="btn btn-outline-danger" data-toggle="collapse" href="#wrong18" role="button">
                                            Mitte midagi.
                                        </a>
                                    </p>
                                    <div id="accordion18">
                                        <div class="collapse" id="right18" data-parent="#accordion18">
                                            <div class="card card-body">
                                                Õige! Ta katsugu järele, kas sa oled ikka liha ja veri, usu Jumala sõna, mis ütleb:
                                                 "Ma tean, et minus, see on minu lihas, ei ole head" (Rm 7:18). 
                                                 Pealegi märkab ta, et on maailmas, mille kohta Jumala sõna ütleb: 
                                                 "Kõik, mis on maailmas, lihahimu, silmahimu ja elukõrkus, ei ole Isast, 
                                                 vaid on maailmast" (1Jh 2:16). Inimese juures on ka alati saatan, 
                                                 kes oma valede ja kurjusega sind alailma meelitab, nagu Piibelgi ütleb: 
                                                 "Olge kained, valvake, sest teie vastane, kurat, käib ümber nagu 
                                                 möirgaja lõukoer otsides, keda neelata. Tema vastu seiske kindlad 
                                                 usus ning teadke, et teie vendadel maailmas tuleb neidsamu kannatusi 
                                                 täielikult kannatada" (1Pt 5:8-9).
                                            </div>
                                        </div>
                                        <div class="collapse" id="wrong18" data-parent="#accordion18">
                                            <div class="card card-body">
                                                Vale! Proovi uuesti!
                                            </div>
                                        </div>
                                    </div>
                               </div>
                                <div class = "modal-footer">
                                    <button type = "button" class = "btn btn-default" data-dismiss = "modal">
                                        Tagasi
                                    </button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </div>
            </div>
        </div>
        <div class="row" style="padding: 1.5em;">
            <p>Armulaud on luterliku kiriku teine sakrament. Seal võtab inimene 
            pühitsetud armulaua leiva ja veini kujul vastu Jeesuse Kristuse ihu 
            ja verd, s.t teda ennast. Armulaud on erinevalt ristimisest mitmekordne 
            sakrament ja sellest on soovitatav osa võtta nii sageli kui võimalik. 
            Armulaud on kirikuliikme vaimulik teemoon, roog, mis on vajalik, 
            et kasvada Jumala tundmises ja kogemises.</p>
            <h4>Kes võib armulauale tulla?</h4>
            <p>Armulauale võib tulla iga ristitud ja konfirmeeritud kiriku liige. 
            On oluline, et inimene oleks teadlik, millega on armulaua puhul tegemist. 
            Ristitud laps võib tulla armulauale koos oma vanemate või ristivanematega, 
            kui talle on selgitatud armulaua tähendust. Armulauale võib ristitud 
            ja konfirmeeritud kiriku liige minna igasse luterlikku kirikusse Eestis 
            ja välismaal. Armulaud ei maksa midagi.</p>
            <h4>Kuidas armulauda vastu võtta?</h4>
            <p>Armulaua vastuvõtmiseks on erinevaid viise ja vahel sõltuvad 
            need koguduse tavadest. Enamasti tullakse luterlikes kirikutes 
            armulauaks altari ette ja põlvitatakse. Suurema rahvahulga puhul 
            toimub armulaua jagamine vahel ka elavas järjekorras.</p>
            <p>Armulaualeiva võib lastada vaimulikul asetada keelele 
            või käele. Käele asetatud leib asetatakse oma käega suhu 
            või kastetakse karikasse ja asetatakse seejärel suhu. Oluline 
            on olla armulaual keskendunud ja käituda väärikalt.</p>
            <h4>Kas armulaud on hügieeniline?</h4>
            <p>Armulauda pühitsev ja jagav vaimulik on kohustatud tagama 
            armulaua hügieeni. Armulauaks tarvitatavad karikad ja taldrikud 
            peavad olema puhtad, varutud on piisav kogus armulauatarvete 
            puhastamiseks vajalikke linikuid, et jagamisel osalevate inimeste 
            käed oleks puhtad ja eelnevalt desinfitseeritud.</p>
            <p>Armulaua karika serva puhastatakse hoolikalt ning jälgitakse, et kogu 
            toiming oleks kaunis, väärikas ja hügieeniline. Kelle jaoks ühisest 
            karikast joomine tundub raske, saab oma leiva katta karikasse.</p>
            <h4>Kuidas saada viimset armulauda?</h4>
            <p>Sellist mõistet nagu “viimne armulaud” meie kirikus ei tunta. 
            Küll on aga olemas haigete armulaud, mida toimetatakse haiglates, 
            hooldekodudes, haigete kodudes ja mujal, kus on inimesi, kes oma 
            tervislikel põhjustel ei saa kirikusse armulauale tulla. Sel 
            juhul on sobilik kutsuda vaimulik koju armulauda pühitsema. 
            Selle kohta vaata lähemalt lehelt Haigete ja surijate võidmine.</p>
        </div>
    </div>
    @endif
@endsection